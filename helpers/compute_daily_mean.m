function [ B, days ] = compute_daily_mean( t, A )
%COMPUTE_DAILY_MEAN Go through data A and calculate means for each day
%
%   INPUTS:
%       t - time records (as serial datenums).
%       A - array of data. can be any number of dimensions, as long as the
%           size of the last dimension equals the number of time records.

dims = size(A);
if length(t) ~= size(A, length(dims))
    error('time must be the last dimension in the original array A')
end

days = datenum(datestr(t,'yyyymmdd'),'yyyymmdd');
maxRecordsPerDay = sum(days == mode(days));
[days,~,ib] = unique(days,'stable');
dims = num2cell(dims(1:end-1));
dims(end+1) = {length(days)};
dims(end+1) = {maxRecordsPerDay};

B = nan(dims{:});
for i = 1:length(days)
    iDay = ib==i;
    B(:,:,i,1:sum(iDay)) = A(:,:,iDay);
end

B = nanmean(B,length(dims));

end

