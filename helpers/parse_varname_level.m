function [varname, level] = parse_varname_level(varname, varargin)
%PARSE_VARNAME_LEVEL Given a string of the form '%s@%s', parse what's
%before the @ as the variable name and what's after the @ as the
%pressure level (converted to int).

% if there's a comma in the varname, just look at everything before the comma
hasComma = strfind(varname,',');
if ~isempty(hasComma)
	varname = varname(1:hasComma-1);
end

level = [];
varnameContainsAt = strfind(varname,'@');
if ~isempty(varnameContainsAt)
    level = varname(varnameContainsAt+1 : end);
    varname = varname(1 : varnameContainsAt-1);
end

if ~isempty(level)
	if nargin >= 2 && varargin{1}
		% convert level to a string
		level = [level '-mb'];
	else
		level = str2num(level);
	end
end

end

