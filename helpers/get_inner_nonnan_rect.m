function [innerRect, varargout] = get_inner_nonnan_rect(arr, varargin)
%GET_INNER_NONNAN_RECT Given a 3D array, returns a new array with the first
%2 dimensions trimmed down to just encompass non-NaN data. Occurrences of
%NaN for each 2D array must be the same.
%
% INPUTS
%   arr:    3D array
%   varargin
%       (1) labels corresponding to rows in arr (e.g. longitudes)
%       (2) labels corresponding to columns in arr (e.g. latitudes)

for k = 1 : size(arr, 3)
    nansPerRow = sum(isnan(arr(:,:,k)), 2);
    rowNotNan = find(nansPerRow == 0, 1, 'first');
    if isempty(rowNotNan)
        rowNotNan = 1;
        for j = 1 : size(arr,2)
            rowInColNotNan = find(~isnan(arr(:,j,k)),1,'first');
            if ~isempty(rowInColNotNan)
                rowNotNan = max(rowNotNan, rowInColNotNan);
            end
        end
    end
    rowNan = find(nansPerRow == 0, 1, 'last');
    if isempty(rowNan)
        rowNan = 0;
        for j = 1 : size(arr,2)
            rowInColNan = find(~isnan(arr(:,j,k)),1,'last');
            if ~isempty(rowInColNan)
                rowNan = min(rowNan, rowInColNan);
            end
        end
    end

    nansPerCol = sum(isnan(arr(rowNotNan : rowNan,:,k)), 1);
    colNotNan = find(nansPerCol == 0, 1, 'first');
    if isempty(colNotNan)
        colNotNan = 1;
        for i = rowNotNan : rowNan
            colInRowNotNan = find(~isnan(arr(i,:,k)),1,'first');
            if ~isempty(colInRowNotNan)
                colNotNan = max(colNotNan, colInRowNotNan);
            end
        end
    end
    colNan = find(nansPerCol == 0, 1, 'last');
    if isempty(colNan)
        colNan = 0;
        for i = rowNotNan : rowNan
            colInRowNan = find(~isnan(arr(i,:,k)),1,'last');
            if ~isempty(colInRowNan)
                colNan = min(colNan, colInRowNan);
            end
        end
    end
    
    if k == 1
		innerRect = zeros(rowNan - rowNotNan + 1, colNan - colNotNan + 1, size(arr,3));
        if length(varargin) == 2
            x = varargin{1}; y = varargin{2};
            varargout{1} = x(rowNotNan : rowNan);
            varargout{2} = y(colNotNan : colNan);
        end
    end
    
    try
        innerRect(:,:,k) = arr(rowNotNan : rowNan, colNotNan : colNan, k);
    catch e
        k
        [rowNan - rowNotNan, colNan - colNotNan]+1
        size(innerRect)
        rethrow(e)
    end
end
    
end

