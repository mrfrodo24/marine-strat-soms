if ~exist('baseLat','var') || ~exist('baseLon','var')
    baseLat = lat;
    baseLon = lon;
end

if ~exist('coastlat','var') || ~exist('coastlon','var')
    load coastlines
end

nx = length(baseLat);  % this represents number of longitudes points
ny = length(baseLon);  % this represents number of latitude points
if exist('fullLat','var')
    latS = min(fullLat);
    latN = max(fullLat);
else
    latS = min(baseLat);
    latN = max(baseLat);
end
if exist('fullLon','var')
    lonW = min(fullLon);
    lonE = max(fullLon);
else
    lonW = min(baseLon);
    lonE = max(baseLon);
end

%SOM size
% Provided by somDims variable specified in user_params.m

% filter out zeros from codebook
cb = sMap.codebook;
cb(cb == 0) = NaN;
if flipCodebook
    cb = flipud(cb);
end

num_nodes = size(cb, 1);

% Take codebook and reshape to display in geographical form
map = csom_output_reshape(cb, num_nodes, nx, ny);

%Use following to get rid of excessive anomaly values
% map(map>=1.0)=1.0;
% map(map<=-1.0)=-1.0;