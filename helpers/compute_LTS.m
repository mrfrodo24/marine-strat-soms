function [ LTS ] = compute_LTS(theta0, theta700)
%COMPUTE_LTS Calculate lower tropospheric stability (LTS) in Kelvin

LTS = theta700 - theta0;

end

