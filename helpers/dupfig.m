% See if there is currently a figure window open

% If so, create a new figure with the same position as the current one

if ~isempty(findall(0,'type','figure'))
    tmph = gcf;
    figure
    set(gcf,'Position',get(tmph, 'Position'))
end