% Make sure have raw dates variable 'd' for each day of data
get_dates_for_matching;

%% Get cloud-eroding boundary dataset (yes/no/maybe)

% Requires marine-strat-tools/io to be on path
if ~all(ismember({'d','yes','no','maybe'}, who))
    [bd,yes,no,maybe] = parse_boundaries([marineStratDataPath boundaryClassFile],2,1);
end