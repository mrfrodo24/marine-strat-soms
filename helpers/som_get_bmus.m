%% Calculate best-matching units

% For each record of base data, returns the node that it most closely
% matches to.
[bmu,q] = som_bmus(sMap,base);
if flipCodebook
    bmu = size(sMap.codebook,1) - bmu + 1;
end
h = hits(bmu);
ph = h / sum(h) * 100;