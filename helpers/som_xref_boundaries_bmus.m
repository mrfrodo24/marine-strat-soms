% DEPENDENCIES
%   (1) Must have loaded data saved by som_runner.m

%% Pre-reqs

% ensure BMUs present
if ~exist('bmu','var')
    som_get_bmus;
end

% if loaded data that's at the same time each day, want to make sure we're
% comparing relevant data. e.g. if looking at 22 UTC data, then we want to
% know if there was a boundary on the following day.
prep_boundaries_xref;


%% xref with cloud-eroding boundary yes/maybe/no days
yesday_bmu = bmu(ismember(d,yes));
noday_bmu = bmu(ismember(d,no));
maybe_bmu = bmu(ismember(d,maybe));

for i = 1:size(sMap.codebook,1)
    yesday_hits(i) = sum(yesday_bmu == i);
    noday_hits(i) = sum(noday_bmu == i);
    maybe_hits(i) = sum(maybe_bmu == i);
end