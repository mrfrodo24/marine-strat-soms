function [ EIS ] = compute_EIS(LTS, temp2m, temp700, z700, LCL)
%COMPUTE_LTS Calculate estimated inversion strength (EIS) based on work by
%Wood and Bretherton (2006).
%
%   EIS = LTS - sigma_moist(T_850, 850hPa) * (z_700 - LCL)          (Eq. 4)
%
%   where sigma_moist() is the moist-adiabatic potential temperature
%   gradient as a function of temperature and pressure (see Eq. 5).
%
%   INPUTS:
%       LTS - lower-tropospheric stability in K
%       temp2m - sfc temperature in K
%       temp700 - 700 mb temperature in K
%       z700 - geopotential height in meters
%       LCL - height of LCL in meters
%

temp850 = (temp2m + temp700) ./ 2;

EIS = LTS - sigma_moist(temp850, 850) .* (z700 - LCL);

function [ moistLapseRate ] = sigma_moist(temp, pres)
    % INPUTS:
    %   temp    - temperature in Kelvin
    %   pres    - pressure level of potTemp in hPa
    % OUPUTS:
    %   moistLapseRate in K/m

    Lv = 2.501e6;   % latent heat of vaporization [J/kg]
    Rd = 287.058;   % dry gas constant [J/kg/K]
    Rv = 461.52;    % water vapor gas constant [J/kg/K]
    cp = 1005.7;    % specific heat of dry air at constant pres [J/kg/K]
    g = 9.81;       % gravity [m/s^2]

    ws = calculate_sat_mixing_ratio(temp, pres);
    numerator   = 1 + Lv * ws ./ Rd ./ temp;
    denominator = 1 + Lv^2 * ws ./ Rv ./ temp.^2 ./ cp;
    moistLapseRate = g/cp * (1 - numerator ./ denominator);

end


function [ ws ] = calculate_sat_mixing_ratio(temp, pres)
    % INPUTS:
    %   [temp]erature in Kelvin
    %   [pres]sure in hPa
    % OUTPUTS:
    %   saturation mixing ratio in kg/kg
    
    tempC = temp - 273.15;

    % Clausius-Clapeyron approximated formula
    es = 6.112 * exp(17.67 * tempC ./ (tempC + 243.5));
    
    % convert to saturation mixing ratio
    ws = 0.622*es ./ (pres - es);

end
        

end

