function [ LCL ] = compute_LCL(t, td)
%COMPUTE_LCL Calculate lifted condensation level in meters.
%
% Calculates the LCL based on equation given by Stull (2000).
%   reference: http://codykirkpatrick.com/work/lclheightdraft.pdf
%
% INPUTS:
%   t - surface (or 1000 mb) air temperature (in K or C, must match units of td)
%   td - surface (or 1000 mb) dew point (in K or C, must match units of t)
%
% OUTPUTS:
%   LCL - lifted condensation level in meters

LCL = 125 * (t - td);

end

