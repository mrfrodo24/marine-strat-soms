function [ anomalies ] = compute_running_anomaly( t, data, length_of_mean, normalize, equal_area, lat )
%COMPUTE_RUNNING_ANOMALY Summary of this function goes here
%   Detailed explanation goes here
%
%   Take some data that may be a single value per time, 1 dimension per
%   time, or 2 dimensions per time, and compute the anomalies at each point
%   from a running mean specified by length_of_mean (in days).
%
%   INPUTS:
%       t - array of serial datenums. must match length of last dimension in data.
%       data - the data to compute anomalies for. for t = number of time
%           records, data must be either 1 x t, m x t, or m x n x t
%       length_of_mean - how long to make running average for mean. By
%           default, the mean is centered.
%       normalize - Set to 1 to also normalize the anomalies by standard deviation.
%       equal_area - Set to 1 to make equal area assumption (i.e. multiply
%           by cos(latitude))
%       lat - array of latitudes (if not applicable, set to [])
%       

anomalies = nan(size(data));
datadims = sum(size(data) > 1);

fd = datenum(datestr(t(1),'yyyymmdd'),'yyyymmdd');
ed = find(t >= fd + length_of_mean + 1, 1, 'first');

while ~isempty(ed)
    if t(ed) - fd > length_of_mean * 2
        error(['Found gap(s) in data between %s and %s which are ' ...
            'too large based on the provided length_of_mean'], ...
            datestr(fd), datestr(t(ed)) ...
        );
    end
    
    ed = fd + length_of_mean + 1;
    datarange = t >= fd & t < ed;
    anomrange = t >= fd + length_of_mean / 2 & t < ed - length_of_mean / 2;
    adjustment = 1;
    
    switch datadims
        case 1
            if normalize, adjustment = 1 ./ nanstd(data(datarange)); end
            anomalies(anomrange) = adjustment .* (data(anomrange) - nanmean(data(datarange)));
        case 2
            if equal_area && ~isempty(lat), adjustment = cosd(lat); end
            if normalize, adjustment = adjument ./ nanstd(data(:,datarange), 0, 2); end
            anomalies(:,anomrange) = adjustment .* (data(:,anomrange) - nanmean(data(:,datarange), 2));
        case 3
            if equal_area, adjustment = cosd(lat); end
            if normalize, adjustment = adjustment ./ nanstd(data(:,:,datarange), 0, 3); end
            anomalies(:,:,anomrange) = adjustment .* (data(:,:,anomrange) - nanmean(data(:,:,datarange), 3));
        otherwise
            error('data is larger than 3-D array')
    end
    
    fd = fd + 1;
    ed = find(t >= fd + length_of_mean + 1, 1, 'first');
end


end

