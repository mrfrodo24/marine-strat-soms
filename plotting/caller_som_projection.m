%frequently changed
somRegion = 'SEA'; % SEA or SEP
hourToPlot = 22;
xrefWithBoundaryAnalysis = 1;
% set to 1 to load SOM data from yes-days, 2 to load SOM data from no-days
% otherwise, leave as 0.
boundaryDayFilter = 0;

%less-frequently changed
hostForPlotting = 'rhodess';
somDims = [5 5];
pathToResults = 'results/5to35S';
if 0 % set to 1 to enable zooming
    switch somRegion
    case 'SEA'
        zoomLat = [-22.5 -10];
        zoomLon = [0.5 13];
    case 'SEP'
        zoomLat = [-22.5 -10];
        zoomLon = [-82.5 -70];
    end
end
% fields of somsForProjection
%   col. 1:     id of SOM configuration in somsAvailable
%   col. 2:     hour of data. if more than one hour, then must be multiple
%               SOMs created for the different hours.
%   col. 3:     months of data. in form 'mmm-*'
%   col. 4:     years included in SOM analysis
%   col. 5:     variables to project on SOM
%   col. 6:     set to 1 to show cross-ref'd boundaries as percentages
% som_projections_lists;
somsForProjection = {...
    % for thesis APPENDIX F
%     'v@975',    [22], 'Apr-May-Jun',    [2007 2017], {'v@975','blh'}, 1; ...
%     'sst',      [22], 'Apr-May-Jun',    [2007 2017], {'sst','blh'}, 1; ...
%     'mslp',     [22], 'Apr-May-Jun',    [2007 2017], {'mslp'}, 1; ...
%     'lts',      [22], 'Apr-May-Jun',    [2007 2017], {'lts'}, 1; ...
%     'u@950',    [22], 'Apr-May-Jun',    [2007 2017], {'u@950'}, 1; ...
%     'z@950',    [22], 'Apr-May-Jun',    [2007 2017], {'z@950'}, 1; ...
%     'z@850',    [22], 'Apr-May-Jun',    [2007 2017], {'z@850'}, 1; ...
%     'z@500',    [22], 'Apr-May-Jun',    [2007 2017], {'z@500'}, 1; ...
%     'rh@800',   [22], 'Apr-May-Jun',    [2007 2017], {'rh@800'}, 1; ...
%     'rh@750',   [22], 'Apr-May-Jun',    [2007 2017], {'rh@750'}, 1; ...
%     'blh',      [22], 'Jan-Feb',        [2008 2017], {'blh'}, 0; ...
%     'blh',      [22], 'May',            [2007 2017], {'blh'}, 0; ...
    'blh',      [22], 'Apr-May-Jun',    [2007 2017], {'v@975'}, 1; ...%'blh','sst','mslp','eis','wind-vec@975'}, 1; ...
    'blh-full', [22], 'Apr-May-Jun',    [2007 2017], {'v@975'}, 1; ...%'blh','sst','mslp','eis','wind-vec@975'}, 1; ...
%     'blh',      [22], 'Oct-Nov-Dec',    [2007 2017], {'blh'}, 1; ...
    
%     'blh-full',       [22], 'Apr-May-Jun', [2007 2017], {'div@975'}, 1; ...
%     'div@975',        [22], 'Apr-May-Jun', [2007 2017], {'wind-vec@975'}, 1; ...
%     'div@975',        [22], 'May',         [2007 2017], {'wind-vec@975'}, 0; ...
%     'div@975',        [22], 'Jan-Feb',     [2008 2017], {'blh','wind-vec@975'}, 0; ...
    % 'blh',       [22], 'Jan-Feb',     [2008 2017], {'div@975'}, 0; ...
%     'blh',      [22], 'Apr-May-Jun',    [2007 2017], {'sst'}, 1; ...
%     'blh-full', [22], 'Apr-May-Jun',    [2007 2017], {'sst'}, 1; ...
};
%     'blh',      [22], 'Jan-Feb',        [2008 2017], {'blh-ew-grad'}, 0; ...
%     'blh-full',      [22], 'Apr-May-Jun',    [2007 2017], {'blh'}, 1; ...
%     'eis-full',      [22], 'Apr-May-Jun',    [2007 2017], {'eis'}, 1; ...
%     'v@900',    [22], 'May',            [2007 2017], {'v@900','blh','wind-vec@900'}, 0; ...
%     'v@900',    [22], 'Apr-May-Jun',    [2007 2017], {'v@900','blh','wind-vec@900'}, 1; ...
%     'v@950',    [22], 'May',            [2007 2017], {'v@950','blh','wind-vec@950'}, 0; ...
%     'v@950',    [22], 'Apr-May-Jun',    [2007 2017], {'v@950','blh','wind-vec@950'}, 1; ...
%     'v@1000',   [22], 'May',            [2007 2017], {'v@1000','blh','wind-vec@1000'}, 0; ...
%     'v@1000',   [22], 'Apr-May-Jun',    [2007 2017], {'v@1000','blh','wind-vec@1000'}, 1; ...
%     'v@925',    [22], 'Jan-Feb-Mar-Apr-May-Jun-Jul-Aug-Sep-Oct-Nov-Dec', [2008 2017], {'v@925'}, 1; ...
%     'v@950',    [22], 'Jan-Feb-Mar-Apr-May-Jun-Jul-Aug-Sep-Oct-Nov-Dec', [2008 2017], {'v@950'}, 1; ...
%     'v@975',    [22], 'Jan-Feb-Mar-Apr-May-Jun-Jul-Aug-Sep-Oct-Nov-Dec', [2008 2017], {'v@975'}, 1; ...
%     'v@925',    [22], 'May',            [2007 2017], {'v@925','blh','wind-vec@925'}, 0; ...
%     'v@925',    [22], 'Apr-May-Jun',    [2007 2017], {'v@925','blh','wind-vec@925'}, 1; ...
%     'v@975',    [22], 'May',            [2007 2017], {'v@975','blh','wind-vec@975'}, 0; ...

%% SOMs that can be created by run_som_configuration.m
somsAvailable = get_available_som_vars();

%% Run analyses

numSomsToAnalyze = size(somsForProjection,1);

for iSom = 1 : numSomsToAnalyze
    
    if strfind(somsForProjection{iSom,1}, '-full')
        somUsedFullValues = 1;
        somsForProjection{iSom,1} = strrep(somsForProjection{iSom,1},'-full','');
    else
        clear somUsedFullValues
    end

    if ~ismember(somsForProjection{iSom,1}, somsAvailable)
        fprintf('Warning: variable "%s" not available for projecting on. Skipping...\n', somsForProjection{iSom,1});
        continue;
    end
    
    somVarId = find(ismember(somsAvailable, somsForProjection{iSom,1}));

    baseSom = sprintf('%s, %s', somsAvailable{somVarId}, somsForProjection{iSom,3});
	whichHours = somsForProjection{iSom,2};
    yearRange = somsForProjection{iSom,4};
    showBoundaryXrefInPercent = xrefWithBoundaryAnalysis && somsForProjection{iSom,6};

    baseVarname = baseSom(1 : strfind(baseSom,',')-1);
    fullvarStr = baseVarname;
    if exist('somUsedFullValues','var') && somUsedFullValues
        fullvarStr = [baseVarname '-full'];
    end
    hourStr = strjoin(strsplit(num2str(whichHours)), '.');
    monthStr = baseSom(strfind(baseSom,',')+2 : end);
    yearStr = sprintf('%i-%i', yearRange(1), yearRange(2));

    baseFilename = sprintf('som_%ix%i_base_%s_%sZ_%s_%s_%s', somDims(1), somDims(2), ...
    	somRegion, hourStr, fullvarStr, monthStr, yearStr);
    switch boundaryDayFilter
    case 1, baseFilename = [baseFilename '_yesdays'];
    case 2, baseFilename = [baseFilename '_nodays'];
    end

    load([baseFilename '.mat']);
    if hourStr(end) == 'Z', hourStr(end) = ''; end

    baseLat = lat;
    baseLon = lon;

    [datapath,ncfile,~,~,sompakPath, ...
        marineStratToolsPath,marineStratDataPath] = ...
        reset_user_paths(hostForPlotting, region, SFC_FIRSTFILE);
    
    % if projecting SSTs (or any other variable only available over ocean /
    % land), will refer back to original lats/lons and embed plot in that
    if ~isempty(strfind(baseVarname, 'sst'))
        ncfilepath = [datapath ncfile];
        fullLat = double(ncread(ncfilepath, LATITUDE));
        fullLon = double(ncread(ncfilepath, LONGITUDE));
    end
    
    som_preanalysis;

    som_get_bmus;
    if xrefWithBoundaryAnalysis
        som_xref_boundaries_bmus;
    else
        clear yesday_hits noday_hits maybe_hits 
    end

    % if need to reorganize map, do so here (e.g. flip the codebook)
    % newOrder = [21:25, 16:20, 11:15, 6:10, 1:5];
    % map = map(:,:,newOrder);
    % if exist('yesday_hits','var')
    %     yesday_hits = yesday_hits(newOrder);
    %     noday_hits = noday_hits(newOrder);
    %     maybe_hits = maybe_hits(newOrder);
    %     newBmu = bmu;
    %     for i = 1:length(newOrder)
    %         newBmu(bmu == i) = newOrder(i);
    %     end
    %     bmu = newBmu;
    % end
        
    if isfield(settings, 'runWithFullValues') && settings.runWithFullValues
        fullAbbreviation = ['Full ' get_short_varname(baseVarname)];
        somProjectionTitleSuffix = strrep(somProjectionTitleSuffix, ...
            get_short_varname(baseVarname), ...
            fullAbbreviation ...
        );
    end
    
    % loop through variables to project
    varsToProject = somsForProjection{iSom,5};
    for j = 1 : length(varsToProject)
        
        isSlimPlots = max(lat(:)) - min(lat(:)) >= max(lon(:)) - min(lon(:));
        if showBoundaryXrefInPercent && isSlimPlots
            som_fig('slim-large');
        elseif showBoundaryXrefInPercent
            som_fig('large');
        elseif isSlimPlots
            som_fig('slim');
        else
            som_fig;
        end
        
        %% Load projection variable
        [projectionVarname,~] = parse_varname_level(varsToProject{j});
        projectionVarFile = sprintf('varToProject_%s_%sZ_%s_%s_%s.mat', ...
            somRegion, hourStr, varsToProject{j}, monthStr, yearStr);
        
        clear lat lon
        load(projectionVarFile);
        if ~exist('lat','var') && ~exist('lon','var')
            lat = baseLat;
            lon = baseLon;
        end
        
        % if loaded wind-vec, also load base variable
        if strcmp(projectionVarname, 'wind-vec')
            load(sprintf('varToProject_%s_%sZ_%s_%s_%s.mat', ...
                somRegion, hourStr, fullvarStr, monthStr, yearStr), 'varToProject');
        end
        
        % subset projection data by yes/no-days if necessary
        if boundaryDayFilter && length(t) < length(tp)
            get_dates_for_matching;
            dp = datenum(datestr(tp,'yyyymmdd'),'yyyymmdd');
            tp = tp(ismember(dp,d-addDayForBoundaryComparison));
            if strcmp(varsToProject{j}, 'wind-vec')
                uToProject = uToProject(:,:,ismember(dp,d-addDayForBoundaryComparison));
                vToProject = vToProject(:,:,ismember(dp,d-addDayForBoundaryComparison));
            end
            varToProject = varToProject(:,:,ismember(dp,d-addDayForBoundaryComparison));
        end
        
        if exist('zoomLat','var') && exist('zoomLon','var')
            zoomLatFilter = lat >= zoomLat(1) & lat <= zoomLat(2);
            zoomLonFilter = lon >= zoomLon(1) & lon <= zoomLon(2);
            lat = lat(zoomLatFilter);
            lon = lon(zoomLonFilter);
            if strcmp(varsToProject{j}, 'wind-vec')
                uToProject = uToProject(zoomLonFilter,zoomLatFilter,:);
                vToProject = vToProject(zoomLonFilter,zoomLatFilter,:);
            end
            varToProject = varToProject(zoomLonFilter,zoomLatFilter,:);
        end
        
        figTitle = [get_long_varname(varsToProject{j}) ' ' somProjectionTitleSuffix];
        
        %% create projection plot
        clear varRange
        som_projection;
        
        clear varToProject uToProject vToProject

        %% save projection plot
        resultsFolder = sprintf('%s/%s_%s_%s_%sZ/', pathToResults, ...
            somRegion, monthStr, yearStr, hourStr);
        if ~exist(resultsFolder,'dir')
            mkdir(resultsFolder);
        end
        
        plotFilename = sprintf('som_%ix%i_%s_%sZ_%s_%s', somDims(1), somDims(2), ...
            somRegion, hourStr, fullvarStr, varsToProject{j});

        switch boundaryDayFilter
            case 1, plotFilename = [plotFilename '_yesdays'];
            case 2, plotFilename = [plotFilename '_nodays'];
        end
        if exist('zoomLat','var') && exist('zoomLon','var')
            plotFilename = [plotFilename '_zoomed'];
        end
        
        plotFilename = sprintf('%s_%s_%s', plotFilename, monthStr, yearStr);
        
        print(gcf, [resultsFolder plotFilename '.png'], '-dpng')
        close(gcf)

    end
    
end