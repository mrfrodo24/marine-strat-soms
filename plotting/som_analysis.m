%% Pre-reqs
% Must have run
%   (1) som_runner.m
%   (2) som_xref_boundaries_bmus.m

%% Calculate some things for analysis
if ~exist('map','var')
    som_preanalysis;
end

%% Diagnostics
% Display the ranges of values for each node
for i = 1:num_nodes
   aa = map(:,:,i);
   fprintf('Node %i range [%.2f, %.2f]\n', i, min(aa(:)), max(aa(:)));
end

%% Change plot padding & margin
padding = 0.003;    % similar to spacing, changes size of individual plots to make padding between them bigger/smaller
margin = 0.04;      % size of margins around entire set of plots
marginLeft = 0.04;   % size of left margin (where colorbar is plotted, if applicable)
spacing = 0.02;     % spacing between subaxes
subplotHeight = 125;% height in pixels of subplot, used to move title below plot

%% Change positioning of yes-no-maybe textbox
% change this if need textbox annotations to change starting position
% default for default figure window
if somDims(1) == 5 && somDims(2) == 5
    if somUsedFullValues
        marginLeft = 0.1;
        spacing = 0.025;
        tbWidth = 0.177;
        tbPositionXOff = 0.242;
        tbHeight = 0.189;
        tbPositionYOff = 0.75;
        if exist('showBoundaryXrefInPercent','var') && showBoundaryXrefInPercent
            tbPositionXOff = 0.24;
            tbPositionYOff = 0.74;
            subplotHeight = 140;
        end
    else
        tbWidth = 0.189;
        tbPositionXOff = 0.195;
        tbHeight = 0.189;
        tbPositionYOff = 0.78;
        spacing = 0.025;
        if exist('showBoundaryXrefInPercent','var') && showBoundaryXrefInPercent
            tbPositionXOff = 0.189;
            tbPositionYOff = 0.74;
            subplotHeight = 140;
        end
    end
elseif somDims(1) == 4 && somDims(2) == 4
    tbWidth = 0.235;
    tbPositionXOff = 0.243;
    tbHeight = 0.235;
    tbPositionYOff = 0.76;
elseif somDims(1) == 3 && somDims(2) == 4
    tbWidth = 0.315;
    tbPositionXOff = 0.285;
    tbHeight = 0.235;
    tbPositionYOff = 0.76;
elseif somDims(1) == 3 && somDims(2) == 3
    tbHeight = 0.315;
    tbPositionYOff = 0.73;
    tbPositionXOff = 0.31;
    tbWidth = 0.318;
    spacing = 0.035;
elseif somDims(1) == 2 && somDims(2) == 3
    tbHeight = 0.315;
    tbPositionYOff = 0.73;
    tbPositionXOff = 0.395;
    tbWidth = 0.475;
    spacing = 0.03;
end
titleOffset = 0.05;
% tbPositionXOff = 0.2;
% tbPositionYOff = 0.5;
% tbWidth = 0.188;
% titleOffset = 0;

if exist('isSlimPlots','var') && isSlimPlots
    tbPositionXOff = tbPositionXOff - 0.004;
end

%% Make base data plot of nodes
clf
%figure(2);
showCoastline = 'y'; % set to abbreviated color for coastline to plot
if somUsedFullValues
    colormap parula
    if max(map(:)) > 100
        upperLimit = ceil((nanmean(map(:)) + 2.5*nanstd(map(:)))/10)*10;
        lowerLimit = floor((nanmean(map(:)) - 2.5*nanstd(map(:)))/10)*10;
    else
        upperLimit = ceil(nanmean(map(:)) + 2.5*nanstd(map(:)));
        lowerLimit = floor(nanmean(map(:)) - 2.5*nanstd(map(:)));
    end
    varDashes = strfind(varname,'-');
    switch varname(1 : varDashes(end)-1)
        case 'eis'
            colormap(flipud(winter))
            varRange = 0:10;
        case 'lts'
            colormap(flipud(winter))
        case 'blh'
            upperLimit = 1600;
            if lowerLimit < 0
                lowerLimit = 0;
            end
            showCoastline = 'r';
        case 'sst'
            showCoastline = 'k';
        case 'modis-peakLwc'
            showCoastline = 'r';
            colormap(flipud(parula))
            varRange = 0:0.05:0.6;
        case 'modis-lwp'
            showCoastline = 'r';
            colormap(flipud(parula))
            upperLimit = 80;
            lowerLimit = 0;
    end
    if ~exist('varRange','var')
        rangeStep = ceil((upperLimit - lowerLimit) / 20);
        varRange = lowerLimit : rangeStep : upperLimit;
    end
else
    % default analysis is for a SOM trained on normalized anomalies
    colormap(redbluecmap(10))
    varRange = -1.0 : 0.2 : 1.0;
end

if xrefWithBoundaryAnalysis
    [~, ~, yesPerNodeRange, noPerNodeRange] = ...
        som_montecarlo(h, yesday_hits, noday_hits);
end

for i = 1:num_nodes
    subaxis(somDims(2),somDims(1),i, 'Spacing', spacing, 'Padding', padding, ...
        'MarginLeft', marginLeft, ...
        'MarginRight', margin, ...
        'MarginTop', margin, ...
        'MarginBottom', margin);
    ax = worldmap([latS latN],[lonW lonE]);
    if length(lat) < length(baseLat) || length(lon) < length(baseLon)
        thisMap = nan(length(baseLat), length(baseLon));
        thisMap(ismember(baseLat,lat), ismember(baseLon,lon)) = map(:,:,i)';
    else
        thisMap = map(:,:,i);
    end
    
    % your domain's lat and lon here
    % use your lat and lon vector, and 500mb height map
    contourfm(baseLat,baseLon,thisMap,varRange);
    caxis(varRange([1 end]))
    % set land or sea color
    if showCoastline
        plotm(coastlat, coastlon, showCoastline, 'LineWidth', 1.2)
    end
    mlabel('off');
    plabel('off');
    ha = title(sprintf('n%i (%i hits, %.0f%%)', i, h(i), ph(i)));
    set(ha,'Units','pixels')
    haPos = get(ha,'Position');
    set(ha,'Position',[haPos(1) haPos(2)-subplotHeight])
    if exist('yesday_hits','var')
        if exist('showBoundaryXrefInPercent','var') && showBoundaryXrefInPercent
            yesday_pct = yesday_hits(i) / sum(yesday_hits(:)) * 100;
            noday_pct = noday_hits(i) / sum(noday_hits(:)) * 100;
            maybe_pct = maybe_hits(i) / sum(maybe_hits(:)) * 100;
            boundaryXrefStr = sprintf('%.0f%% y\n%.0f%% n\n%.0f%% m', ...
                yesday_pct, noday_pct, maybe_pct);
        else
            boundaryXrefStr = sprintf('%i y\n%i n\n%i m', ...
                yesday_hits(i), noday_hits(i), maybe_hits(i));
        end
        if yesday_hits(i) >= yesPerNodeRange(1,i)
            boundaryXrefStr = strrep(boundaryXrefStr, 'y', 'y*');
        elseif yesday_hits(i) < yesPerNodeRange(2,i)
            boundaryXrefStr = strrep(boundaryXrefStr, 'y', 'y-');
        end
        if noday_hits(i) >= noPerNodeRange(1,i)
            boundaryXrefStr = strrep(boundaryXrefStr, 'n', 'n*');
        elseif noday_hits(i) < noPerNodeRange(2,i)
            boundaryXrefStr = strrep(boundaryXrefStr, 'n', 'n-');
        end
        h_textbox_ann = annotation('textbox','String',boundaryXrefStr);
        h_textbox_ann.FontSize = 9;
        h_textbox_ann.LineStyle = 'none';
        xoffset = mod(i,somDims(1)); if xoffset == 0, xoffset = somDims(1); end
        xoffset = xoffset - 1;
        yoffset = floor((i-1) / somDims(1));
        tbPosition = [tbPositionXOff+tbWidth*xoffset tbPositionYOff-tbHeight*yoffset 0.0781 0.1345];
        h_textbox_ann.Position = tbPosition;
    end
end

a = axes;
t1 = title(somBasePlotTitle,'FontSize',14);
a.Visible = 'off';
t1.Position(2) = t1.Position(2) + titleOffset;
t1.Visible = 'on';
if somUsedFullValues
    ch = colorbar();
    ch.Position(1) = 0.05;
    ch.LineWidth = 1;
    caxis(varRange([1 end]))
    a.FontSize = 12;
end


