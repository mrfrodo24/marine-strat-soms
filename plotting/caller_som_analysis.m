somRegion = 'SEA'; % SEA or SEP
hourToPlot = 22;
hostForPlotting = 'rhodess';
somDims = [5 5];
% set to 1 to show num boundary days matching node
xrefWithBoundaryAnalysis = 1;
% set to 1 to load SOM data from yes-days, 2 to load SOM data from no-days
% otherwise, leave as 0.
boundaryDayFilter = 0;
pathToResults = 'results/5to35S';
% fields of somsForAnalysis
%   col. 1:     id of SOM configuration in somsAvailable
%   col. 2:     hour of data. if more than one hour, then must be multiple
%               SOMs created for the different hours.
%   col. 3:     months of data. in form 'mmm-*'
%   col. 4:     years included in SOM analysis
%   col. 5:     set to 1 if loading SOM on full values instead of
%               anomalies. default (0) is normalized anomalies.
%   col. 6:     set to 1 to show cross-ref'd boundaries as percentages
% som_analyses_lists;
somsForAnalysis = {
    'blh',          [22], 'Apr-May-Jun', [2007 2017], 0, 1; ...
    'mslp',         [22], 'Apr-May-Jun', [2007 2017], 0, 1; ...
    
%     'modis-lwp',      [0], 'Apr-May-Jun', [2000 2018], 1, 1; ...
%     'modis-peakLwc',  [0], 'Apr-May-Jun', [2000 2018], 1, 1; ...
%     'modis-lwp',      [0], 'Apr-May-Jun', [2007 2017], 0, 1; ...
%     'modis-peakLwc',  [0], 'Apr-May-Jun', [2007 2017], 0, 1; ...
};

%% SOMs that can be created by run_som_configuration.m
somsAvailable = get_available_som_vars();

%% Run analyses

numSomsToAnalyze = size(somsForAnalysis,1);

for iSom = 1 : numSomsToAnalyze

    if ~ismember(somsForAnalysis{iSom,1}, somsAvailable)
        fprintf('Warning: variable "%s" not available for analysis. Skipping...\n', somsForAnalysis{iSom,1});
        continue;
    end
    
    somVarId = find(ismember(somsAvailable, somsForAnalysis{iSom,1}));

	somToAnalyze = sprintf('%s, %s', somsAvailable{somVarId}, somsForAnalysis{iSom,3});
	whichHours = somsForAnalysis{iSom,2};
    yearRange = somsForAnalysis{iSom,4};
    somUsedFullValues = somsForAnalysis{iSom,5};
    showBoundaryXrefInPercent = xrefWithBoundaryAnalysis && somsForAnalysis{iSom,6};

    varname = somToAnalyze(1 : strfind(somToAnalyze,',')-1);
    monthStr = somToAnalyze(strfind(somToAnalyze,',')+2 : end);
    hourStr = strjoin(strsplit(num2str(whichHours)), '.');
    
    if somUsedFullValues
        varname = [varname '-full'];
    end

    baseFilename = sprintf('som_%ix%i_base_%s_%sZ_%s_%s_%i-%i', somDims(1), somDims(2), ...
    	somRegion, hourStr, varname, monthStr, yearRange(1), yearRange(2));
    switch boundaryDayFilter
    case 1, baseFilename = [baseFilename '_yesdays'];
    case 2, baseFilename = [baseFilename '_nodays'];
    end

    load([baseFilename '.mat']);
    if hourStr(end) == 'Z', hourStr(end) = ''; end
    
    isSlimPlots = max(lat(:)) - min(lat(:)) >= max(lon(:)) - min(lon(:));
    if showBoundaryXrefInPercent && isSlimPlots
        som_fig('slim-large');
    elseif showBoundaryXrefInPercent
        som_fig('large');
    elseif isSlimPlots
        som_fig('slimmer');
    else
        som_fig;
    end

    [datapath,ncfile,~,~,sompakPath, ...
        marineStratToolsPath,marineStratDataPath] = ...
        reset_user_paths(hostForPlotting, region, SFC_FIRSTFILE);

    % if plotting SSTs (or any other variable only available over ocean /
    % land), will refer back to original lats/lons and embed plot in that
    if ~isempty(strfind(varname, 'sst'))
        ncfilepath = [datapath ncfile];
        fullLat = double(ncread(ncfilepath, LATITUDE));
        fullLOn = double(ncread(ncfilepath, LONGITUDE));
    end

    som_preanalysis;
    
    som_get_bmus;
    if xrefWithBoundaryAnalysis
        som_xref_boundaries_bmus;
    else
        clear yesday_hits noday_hits maybe_hits 
    end

    % if need to reorganize map, do so here (e.g. flip the codebook)
%     newOrder = [21:25, 16:20, 11:15, 6:10, 1:5];
%     map = map(:,:,newOrder);
%     if exist('yesday_hits','var')
%         yesday_hits = yesday_hits(newOrder);
%         noday_hits = noday_hits(newOrder);
%         maybe_hits = maybe_hits(newOrder);
%     end

    % update title in case SOM was trained on full values
    if somUsedFullValues
        somBasePlotTitle = ['Full ' strrep(somBasePlotTitle, 'anomalies ', '')];
    end

    som_analysis;
    
    if somUsedFullValues, clear varRange; end

    resultsFolder = sprintf('%s/%s_%s_%i-%i_%sZ/', pathToResults, ...
        somRegion, monthStr, yearRange(1), yearRange(2), hourStr);
    if ~exist(resultsFolder,'dir')
    	mkdir(resultsFolder);
    end
    plotFilename = sprintf('som_%ix%i_%s_%sZ_%s_base', somDims(1), somDims(2), ...
        somRegion, hourStr, varname);
    switch boundaryDayFilter
        case 1, plotFilename = [plotFilename '_yesdays'];
        case 2, plotFilename = [plotFilename '_nodays'];
    end
    plotFilename = sprintf('%s_%s_%i-%i', plotFilename, monthStr, yearRange(1), yearRange(2));

    print(gcf, [resultsFolder plotFilename '.png'], '-dpng')
    
    close(gcf)
    clear map

end