%% Load projection data
[varToProject,tt] = load_som_base(datapath, 'era5_largescale_2007_17_SEA_850_500hpa.nc', 'z', 850);
varToProject = varToProject(:,:,ismember(hour(tt), whichHours) & ismember(month(tt), whichMonths));

%% Variable-specific plotting
% varRange = 5750:10:5900; % 500 mb
varRange = 1500:5:1580; % 850 mb
colormap parula
figTitle = ['850-mb geopotential height (m)' somProjectionTitleSuffix];

%% Do the projection
som_projection;