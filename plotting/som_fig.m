function som_fig(varargin)
% Make a new figure window that's the standard size we'll use for plots of
% the entire SOM (all nodes), since we want these to all have the same size
% for easier comparison and reproducability

fh = figure;
if nargin == 0, sz = 'default';
else, sz = varargin{1}; end
switch sz
    case 'slim-large'
        set(fh, 'Position', [235   42  909.0  782.5])
    case 'large'
        set(fh, 'Position', [235   42  976.5  782.5])
    case 'slim'
        set(fh, 'Position', [260.5  109.5  800  696])
    case 'slimmer'
        set(fh, 'Position', [260.5  109.5  753  696])
    otherwise
        set(fh, 'Position', [260.5  109.5  846  696])
end

end