%% Load projection data
[varToProject,tt] = load_som_base(datapath, ncfile, 't2m', []);
varToProject = varToProject(:,:,ismember(hour(tt), whichHours) & ismember(month(tt), whichMonths));

%% Variable-specific plotting
varRange = 285:300;
colormap parula
figTitle = ['2-meter temperature (K)' somProjectionTitleSuffix];

%% Do the projection
som_projection;