%% Load projection data (temperature)
% 
% varToProject = EIS(:,:,ismember(hour(tt), whichHours) & ismember(month(tt), whichMonths));

%% Alternative load section
load data/data_toproject_EIS.mat

%% Variable-specific plotting
varRange = -5:1:15;
colormap(flipud(winter))
figTitle = ['Estimated inversion strength (K)' somProjectionTitleSuffix];

%% Do the projection
som_projection;