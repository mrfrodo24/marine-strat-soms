%% Load projection data
[varToProject,tt] = load_som_base(datapath, ncfile, 'msl', []);
varToProject = varToProject(:,:,ismember(hour(tt), whichHours) & ismember(month(tt), whichMonths));

%% Variable-specific plotting
varRange = 1010:1030;
colormap parula
figTitle = ['Mean sea level pressure (mb)' somProjectionTitleSuffix];

%% Do the projection
som_projection;