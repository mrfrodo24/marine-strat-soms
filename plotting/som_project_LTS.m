%% Load projection data (temperature)
% ncfile1 = 'era5_largescale_2007_17_SEA_sfc.nc';
% ncfile2 = 'era5_largescale_2007_17_SEA_700hpa.nc';
% [theta0,tt] = load_era5_potential_temperature(datapath, ncfile1, []);
% [theta700,~] = load_era5_potential_temperature(datapath, ncfile2, 700);
% LTS = compute_LTS(theta0, theta700);
% 
% varToProject = LTS(:,:,ismember(hour(tt), whichHours) & ismember(month(tt), whichMonths));

%% Alternative load section
load data/data_toproject_LTS.mat

%% Variable-specific plotting
varRange = 2:2:28;
colormap(flipud(winter))
figTitle = ['Lower-tropospheric stability (K)' somProjectionTitleSuffix];

%% Do the projection
som_projection;