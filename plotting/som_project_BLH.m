%% Load projection data
[varToProject,tt] = load_som_base(datapath, ncfile, 'blh', []);
varToProject = varToProject(:,:,ismember(hour(tt), whichHours) & ismember(month(tt), whichMonths));

%% Variable-specific plotting
varRange = 50:50:1500;
colormap parula
figTitle = ['Boundary layer height (m)' somProjectionTitleSuffix];

%% Do the projection
som_projection;