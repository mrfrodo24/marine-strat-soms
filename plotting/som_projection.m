%% Pre-reqs
% Must have run
%   (1) som_runner.m
%   (2) som_xref_boundaries_bmus.m

%% Calculate some things for analysis
som_preanalysis;

%% Change plot padding & margin
padding = 0.003;    % similar to spacing, changes size of individual plots to make padding between them bigger/smaller
margin = 0.04;      % size of margins around entire set of plots
marginLeft = 0.1;   % size of left margin (where colorbar is plotted by this script)
spacing = 0.025;     % spacing between subaxes
subplotHeight = 120;% height in pixels of subplot, used to move title below plot

%% Change positioning of yes-no-maybe textbox
% change this if need textbox annotations to change starting position
% default for default figure window
if somDims(1) == 5 && somDims(2) == 5
    tbWidth = 0.177;
    tbPositionXOff = 0.242;
    tbHeight = 0.189;
    tbPositionYOff = 0.78;
    if exist('showBoundaryXrefInPercent','var') && showBoundaryXrefInPercent
        tbPositionXOff = 0.24;
        tbPositionYOff = 0.735;
        subplotHeight = 137;
    end
elseif somDims(1) == 4 && somDims(2) == 4
    tbWidth = 0.222;
    tbPositionXOff = 0.286;
    tbHeight = 0.236;
    tbPositionYOff = 0.76;
elseif somDims(1) == 3 && somDims(2) == 4
    tbWidth = 0.295;
    tbPositionXOff = 0.33;
    tbHeight = 0.236;
    tbPositionYOff = 0.76;
elseif somDims(1) == 3 && somDims(2) == 3
    tbWidth = 0.295;
    tbHeight = 0.315;
    tbPositionYOff = 0.72;
    tbPositionXOff = 0.36;
elseif somDims(1) == 2 && somDims(2) == 3
    tbWidth = 0.443;
    tbHeight = 0.315;
    tbPositionYOff = 0.72;
    tbPositionXOff = 0.44;
end
titleOffset = 0.05;
% tbPositionXOff = 0.2;
% tbPositionYOff = 0.5;
% tbWidth = 0.188;
% titleOffset = 0;

if exist('isSlimPlots','var') && isSlimPlots
    tbPositionXOff = tbPositionXOff - 0.004;
    subplotHeight = subplotHeight + 5;
end

%% Plot projected variable
% Range for 500 mb geopotential height
% varRange = 5750:7.5:5900;
% Range for mslp
% varRange = 1010:1030;
% Range for BLH
% varRange = 200:50:1500;
colormap parula
if ~exist('varRange','var')
    if all(ismember({'uToProject','vToProject'}, who))
        % Scale u,v for quiverm later
        uToProject = uToProject ./ 5; % 1 degree longitude will represent 5 m/s
        vToProject = vToProject ./ 5; % 1 degree latitude will represent 5 m/s
    end
    if max(varToProject(:)) > 100
        upperLimit = ceil((nanmean(varToProject(:)) + 2.5*nanstd(varToProject(:)))/10)*10;
        lowerLimit = floor((nanmean(varToProject(:)) - 2.5*nanstd(varToProject(:)))/10)*10;
    elseif max(varToProject(:)) < 1
        upperLimit = ceil((nanmean(varToProject(:)) + 2.5*nanstd(varToProject(:)))*1000)/1000;
        lowerLimit = floor((nanmean(varToProject(:)) - 2.5*nanstd(varToProject(:)))*1000)/1000;
    else
        upperLimit = ceil(nanmean(varToProject(:)) + 2.5*nanstd(varToProject(:)));
        lowerLimit = floor(nanmean(varToProject(:)) - 2.5*nanstd(varToProject(:)));
    end
end
showCoastline = 'r'; % set to abbreviated color for coastline to plot
numContours = 20;
if exist('varsToProject','var')
    % if overlaying wind vectors, use base variable in switch statement
    varBeingProjected = varsToProject{j};
    if exist('baseVarname','var') ...
            && exist('projectionVarname','var') ...
            && strcmp(projectionVarname, 'wind-vec')
        varBeingProjected = baseVarname;
    end
    switch varBeingProjected
        case {'u@900','u@950','u@1000','v@900','v@925','v@950','v@975','v@1000'}
            colormap(redbluecmap(10))
            varRange = -15:3:15;
            showCoastline = 'y';
        case {'div@975'}
            cmap = redbluecmap(20);
            colormap(cmap)
            varRange = -1e-5:1e-6:1e-5;
            % modify varToProject to be within this range
%             varToProject(varToProject < varRange(1)) = varRange(1);
%             varToProject(varToProject > varRange(end)) = varRange(end);
            % set values over land to NaN
            coast = load('coast.mat');
            [meshLon,meshLat] = meshgrid(lon,lat);
            landMask = inpolygon(meshLon,meshLat,coast.long,coast.lat)';
            varToProject(repmat(landMask,1,1,size(varToProject,3))) = NaN;
            showCoastline = 'g';
        case 'mslp'
            varRange = 1010:1.5:1030;
            cmap = get_custom_colormap('inferno', length(varRange)+2, 1);
            colormap(cmap(4:end,:))
            showCoastline = 'w';
        case {'z@1000','z@950','z@900','z@850','z@500'}
            cmap = get_custom_colormap('inferno', 20, 1);
            colormap(cmap(6:end,:))
            showCoastline = 'w';
        case 'eis'
            colormap(flipud(winter))
            varRange = 0:10;
            showCoastline = 'y';
        case 'lts'
            colormap(flipud(winter))
            showCoastline = 'y';
        case 'blh'
            upperLimit = 1600;
            if lowerLimit < 0
                lowerLimit = 0;
            end
        case {'rh@700','rh@750','rh@800'}
            cmap = bone(64);
            colormap(flipud(cmap(12:end,:)))
            lowerLimit = 0;
            upperLimit = 100;
        case 'blh-ew-grad'
            colormap(redbluecmap)
            lowerLimit = -5;
            upperLimit = 5;
            showCoastline = 'y';
            numContours = 10;
        case 'sst'
            varRange = 286:2:302;
            colormap(get_custom_colormap('alt-jet', diff(varRange([1 end]))-1))
            showCoastline = 'k';
    end
    if ~exist('varRange','var')
        limitDiff = upperLimit - lowerLimit;
        if limitDiff < 0.5
            rangeStep = ceil((limitDiff/numContours)*10000)/10000;
        elseif limitDiff < 10
            rangeStep = ceil((limitDiff/numContours)*1000)/1000;
        else
            rangeStep = ceil(limitDiff / numContours);
        end
        varRange = lowerLimit : rangeStep : upperLimit;
    end
end

clf;

if xrefWithBoundaryAnalysis
    [~, ~, yesPerNodeRange, noPerNodeRange] = ...
        som_montecarlo(h, yesday_hits, noday_hits);
end

for i = 1:num_nodes
    subaxis(somDims(2),somDims(1),i, 'Spacing', spacing, 'Padding', padding, ...
        'MarginLeft', marginLeft, ...
        'MarginRight', margin, ...
        'MarginTop', margin, ...
        'MarginBottom', margin);
    ax = worldmap([latS latN],[lonW lonE]);
    % use best-matching units array to get the env'ts which match this node
    dataToProject = varToProject(:,:, bmu == i);
    if size(dataToProject, 3) > 1
        dataToProject = nanmean(dataToProject,3);
    end
    if all(ismember({'uToProject','vToProject'}, who))
        [meshLat,meshLon] = meshgrid(lat(2:6:end),lon(2:6:end));
        % downsample to having a vector every 1.25 degrees
        uData = nanmean(uToProject(2:6:end,2:6:end, bmu == i), 3);
        vData = nanmean(vToProject(2:6:end,2:6:end, bmu == i), 3);
        qh = quiverm(meshLat,meshLon,vData,uData,'-k',0);
    else
        try
            contourfm(lat,lon,dataToProject',varRange)
%             surfm(lat,lon,dataToProject')
        catch e
            warning(e.message)
            disp('Continuing...')
            continue;
        end
        caxis(varRange([1 end]))
    end
    % set land or sea color
    if showCoastline
        plotm(coastlat, coastlon, showCoastline, 'LineWidth', 1.2)
    end
        
    mlabel('off');
    plabel('off');
    ha = title(sprintf('n%i (%i hits, %.0f%%)', i, h(i), ph(i)));
    set(ha,'Units','pixels')
    haPos = get(ha,'Position');
    set(ha,'Position',[haPos(1) haPos(2)-subplotHeight])
    if exist('yesday_hits','var')
        if exist('showBoundaryXrefInPercent','var') && showBoundaryXrefInPercent
            yesday_pct = yesday_hits(i) / sum(yesday_hits(:)) * 100;
            noday_pct = noday_hits(i) / sum(noday_hits(:)) * 100;
            maybe_pct = maybe_hits(i) / sum(maybe_hits(:)) * 100;
            boundaryXrefStr = sprintf('%.0f%% y\n%.0f%% n\n%.0f%% m', ...
                yesday_pct, noday_pct, maybe_pct);
        else
            boundaryXrefStr = sprintf('%i y\n%i n\n%i m', ...
                yesday_hits(i), noday_hits(i), maybe_hits(i));
        end
        if yesday_hits(i) >= yesPerNodeRange(1,i)
            boundaryXrefStr = strrep(boundaryXrefStr, 'y', 'y*');
        elseif yesday_hits(i) < yesPerNodeRange(2,i)
            boundaryXrefStr = strrep(boundaryXrefStr, 'y', 'y-');
        end
        if noday_hits(i) >= noPerNodeRange(1,i)
            boundaryXrefStr = strrep(boundaryXrefStr, 'n', 'n*');
        elseif noday_hits(i) < noPerNodeRange(2,i)
            boundaryXrefStr = strrep(boundaryXrefStr, 'n', 'n-');
        end
        h_textbox_ann = annotation('textbox','String',boundaryXrefStr);
        h_textbox_ann.FontSize = 9;
        h_textbox_ann.LineStyle = 'none';
        xoffset = mod(i,somDims(1)); if xoffset == 0, xoffset = somDims(1); end
        xoffset = xoffset - 1;
        yoffset = floor((i-1) / somDims(1));
        tbPosition = [tbPositionXOff+tbWidth*xoffset tbPositionYOff-tbHeight*yoffset 0.0781 0.1345];
        h_textbox_ann.Position = tbPosition;
    end
end

a = axes;
if exist('figTitle', 'var')
    t1 = title(figTitle);
    t1.Position(2) = t1.Position(2) + titleOffset;
    t1.Visible = 'on';
end
if ~all(ismember({'uToProject','vToProject'}, who))
    ch = colorbar();
    ch.Position(1) = 0.05;
    ch.LineWidth = 1;
    ch.Ticks = varRange(1:2:end);
    caxis(varRange([1 end]))
    a.FontSize = 12;
end
a.Visible = 'off';