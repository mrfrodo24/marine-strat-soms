%% Runner for southeast Atlantic SOMs

%% user params

region = 'SEA'; % must match a valid region in run_som_configuration.m

dimsToUse = [5 5];
% dimsToUse = [ceil(1:0.5:9.5); ceil(1.5:0.5:10)]';
% above line makes dims 1x2, 2x2, 2x3, 3x3, ..., 10x10

% columns in somsToMake
%   1) id of SOM, as a character array. Must match a case in run_som_configuration.m
%   2) dimensions of the SOM(s), each listed set of dims will be a different SOM. 
%       If empty will use default somDims.
%   3) hours to use (each hour listed will be a different SOM)
%   4) year range to use for each SOM configuration
%   5) months to use (each month string listed will be a different SOM)
somsToMake = {...
    'modis-peakLwc',    dimsToUse, [0], [2000 2018], {'Apr-May-Jun'} ; ...
    'modis-lwp',		dimsToUse, [0], [2000 2018], {'Apr-May-Jun'} ; ...
}; %#ok<*NBRAK>

%% run SOMs

if exist('runBatchMode','var') && runBatchMode
    batch_run_som_configuration;
else
    caller_run_som_configuration;
end
