# marine-strat-soms

Analysis of marine stratocumulus with self-organizing maps.  This codebase utilizes the SOM-Toolbox, developed in 1999 and revised and added to GitHub in 2012 (https://github.com/ilarinieminen/SOM-Toolbox).  Analyses herein seek to examine differences in environments on days with/without cloud-eroding boundaries or between seasons when they are more/less frequent.