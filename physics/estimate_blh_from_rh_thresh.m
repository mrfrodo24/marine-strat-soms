function [ blh ] = estimate_blh_from_rh_thresh(rh, z)
%ESTIMATE_BLH_FROM_RH_THRESH Estimate height of the boundary layer by
%interpolating to find the level where RH is 70%
%
% INPUTS:
%   rh
%       The relative humidities [%], expected to be a 4-D, where the 3rd
%       dimension is levels and the 4th dimension is time
%   z
%       The geopotential heights [m], expected to be same size as rh
%

arraySize = size(rh);
blh = nan(arraySize([1 2 4]));

for t = 1 : size(rh,4)
    for i = 1 : size(rh,1)
        for j = 1 : size(rh,2)
            rhColumn = squeeze(rh(i,j,:,t));
            zColumn = squeeze(z(i,j,:,t));
            
            % first find the level of max RH in case cloud layer is decoupled
            [maxRh,maxId] = max(rhColumn);
            if maxRh < 70
                % nothing we can do here
                continue;
            else
                maxId = maxId(1);
            end
            
            % look up from the level of max RH for level out of cloud
            threshId = find(rhColumn(maxId:end,1) < 70, 1, 'first');
            
            if isempty(threshId)
                % nothing we can do here
                continue;
            else
                % note that we're adding back maxId but subtracting 1 so
                % that threshId points to the RH that's >= 70%, 
                % and threshId+1 points to the RH that's < 70%
                threshId = threshId(1) + (maxId-1) - 1;
            end
                
            if all([round(rhColumn(threshId)) > 70, threshId < length(rhColumn)])
                % interpolate to get a more exact height
                rhDiff = rhColumn(threshId) - rhColumn(threshId+1);
                pctDiff = (rhColumn(threshId) - 70) ./ rhDiff;
                zDiff = zColumn(threshId+1) - zColumn(threshId);
                blh(i,j,t) = zColumn(threshId) + zDiff * pctDiff;
            else
                blh(i,j,t) = zColumn(threshId);
            end
        end
    end
end

end

