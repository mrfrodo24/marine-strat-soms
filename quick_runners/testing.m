clear
blhRhFile = 'results/testing/201405%02.f_blh-rh.png';
blhEraFile = 'results/testing/201405%02.f_blh-era5.png';
blhBiasFile = 'results/testing/201405%02.f_blh-rh-bias.png';

load coastlines

for i = 1:31
    load('tmp_blh-rh-v2_May2014.mat')

    figure('Position', [38.5000   87.5000  731.0000  663.5000])
    contourf(lon,lat,blh(:,:,i)',0:200:1600)
    hold on
    plot(coastlon,coastlat,'r','LineWidth',2)
    caxis([0 1600])
    xlim([-10 20])
    ylim([-35 -5])
    set(gca,'FontSize',14)
    ch = colorbar();
    ylabel(ch, '[m]')
    ylabel('deg N')
    xlabel('deg E')
    title(['BLH using RH threshold - 22Z May ' num2str(i) ', 2014'])
    print(gcf, sprintf(blhRhFile, i), '-dpng')


    load('tmp_blh_May2014.mat')

    dupfig
    contourf(lon,lat,blh(:,:,i)',0:200:1600)
    hold on
    plot(coastlon,coastlat,'r','LineWidth',2)
    caxis([0 1600])
    xlim([-10 20])
    ylim([-35 -5])
    set(gca,'FontSize',14)
    ch = colorbar();
    ylabel(ch, '[m]')
    ylabel('deg N')
    xlabel('deg E')
    title(['BLH from ERA5 - 22Z May ' num2str(i) ', 2014'])
    print(gcf, sprintf(blhEraFile, i), '-dpng')


    blhEra = blh;
    load('tmp_blh-rh-v2_May2014.mat', 'blh');

    dupfig
    colormap(redblue)
    contourf(lon,lat,blh(:,:,i)' - blhEra(:,:,i)', -500:50:500)
    hold on
    plot(coastlon,coastlat,'g','LineWidth',2)
    caxis([-500 500])
    xlim([-10 20])
    ylim([-35 -5])
    set(gca,'FontSize',14)
    ch = colorbar();
    ylabel(ch, '[m]')
    ylabel('deg N')
    xlabel('deg E')
    title(['BLH from RH - ERA5 BLH: 22Z May ' num2str(i) ', 2014'])
    print(gcf, sprintf(blhBiasFile, i), '-dpng')
    
    close all
end