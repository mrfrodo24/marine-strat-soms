figure(1)
set(gcf,'Position',[248.0000  366.5000  989.0000  371.5000])
yyaxis left
set(gca,'FontSize',12)
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
yyaxis right
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
print(gcf,'results/som_dim_analysis/blh_Aug-Sep_2007-2017.png','-dpng')
figure(2)
set(gcf,'Position',[248.0000  366.5000  989.0000  371.5000])
yyaxis left
set(gca,'FontSize',12)
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
yyaxis right
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
print(gcf,'results/som_dim_analysis/blh_May_2007-2017.png','-dpng')
figure(3)
set(gcf,'Position',[248.0000  366.5000  989.0000  371.5000])
yyaxis left
set(gca,'FontSize',12)
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
yyaxis right
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
print(gcf,'results/som_dim_analysis/blh_Apr-May-Jun_2007-2017.png','-dpng')
figure(4)
set(gcf,'Position',[248.0000  366.5000  989.0000  371.5000])
yyaxis left
set(gca,'FontSize',12)
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
yyaxis right
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
print(gcf,'results/som_dim_analysis/blh_Oct-Nov-Dec_2007-2017.png','-dpng')
figure(5)
set(gcf,'Position',[248.0000  366.5000  989.0000  371.5000])
yyaxis left
set(gca,'FontSize',12)
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
yyaxis right
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
print(gcf,'results/som_dim_analysis/sst_Aug-Sep_2007-2017.png','-dpng')
figure(6)
set(gcf,'Position',[248.0000  366.5000  989.0000  371.5000])
yyaxis left
set(gca,'FontSize',12)
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
yyaxis right
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
print(gcf,'results/som_dim_analysis/sst_May_2007-2017.png','-dpng')
figure(7)
set(gcf,'Position',[248.0000  366.5000  989.0000  371.5000])
yyaxis left
set(gca,'FontSize',12)
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
yyaxis right
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
print(gcf,'results/som_dim_analysis/v@1000_Aug-Sep_2007-2017.png','-dpng')
figure(8)
set(gcf,'Position',[248.0000  366.5000  989.0000  371.5000])
yyaxis left
set(gca,'FontSize',12)
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
yyaxis right
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
print(gcf,'results/som_dim_analysis/v@1000_May_2007-2017.png','-dpng')
figure(9)
set(gcf,'Position',[248.0000  366.5000  989.0000  371.5000])
yyaxis left
set(gca,'FontSize',12)
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
yyaxis right
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
print(gcf,'results/som_dim_analysis/v@1000_Oct-Nov-Dec_2007-2017.png','-dpng')
figure(10)
set(gcf,'Position',[248.0000  366.5000  989.0000  371.5000])
yyaxis left
set(gca,'FontSize',12)
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
yyaxis right
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
print(gcf,'results/som_dim_analysis/mslp_Jan-Feb_2008-2017.png','-dpng')
figure(11)
set(gcf,'Position',[248.0000  366.5000  989.0000  371.5000])
yyaxis left
set(gca,'FontSize',12)
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
yyaxis right
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
print(gcf,'results/som_dim_analysis/mslp_Oct-Nov-Dec_2007-2017.png','-dpng')
figure(12)
set(gcf,'Position',[248.0000  366.5000  989.0000  371.5000])
yyaxis left
set(gca,'FontSize',12)
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
yyaxis right
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
print(gcf,'results/som_dim_analysis/z@850_Aug-Sep_2007-2017.png','-dpng')
figure(13)
set(gcf,'Position',[248.0000  366.5000  989.0000  371.5000])
yyaxis left
set(gca,'FontSize',12)
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
yyaxis right
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
print(gcf,'results/som_dim_analysis/z@850_May_2007-2017.png','-dpng')
figure(14)
set(gcf,'Position',[248.0000  366.5000  989.0000  371.5000])
yyaxis left
set(gca,'FontSize',12)
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
yyaxis right
ax = get(gcf,'Children');
ax.Children.SizeData = 64;
ax.Children.LineWidth = 2.25;
print(gcf,'results/som_dim_analysis/z@850_Oct-Nov-Dec_2007-2017.png','-dpng')
