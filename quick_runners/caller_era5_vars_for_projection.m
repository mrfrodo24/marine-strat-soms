
region = 'SEA';
init;
whichHour = 10;

% whichMonths = [5];
% yearRange = [2007 2017];
% varsToGrab = {'z@500','z@850','z@900','z@950','z@1000','mslp','blh','blh-ew-grad',...
% 	'wind-vec@900','wind-vec@950','wind-vec@1000','u@900','u@950','u@1000','v@900','v@950','v@1000','eis'};
% save_era5_vars_for_projection;

% whichMonths = [1 2];
% yearRange = [2008 2017];
% varsToGrab = {'z@500','z@850','z@900','z@950','z@1000','mslp','blh','blh-ew-grad',...
% 	'wind-vec@900','wind-vec@950','wind-vec@1000','u@900','u@950','u@1000','v@900','v@950','v@1000','eis'};
% save_era5_vars_for_projection;

% whichMonths = [4 5 6];
% yearRange = [2007 2017];
% varsToGrab = {'z@500','z@850','z@900','z@950','z@1000','mslp','blh','blh-ew-grad',...
% 	'wind-vec@900','wind-vec@950','wind-vec@1000','u@900','u@950','u@1000','v@900','v@950','v@1000','eis'};
% save_era5_vars_for_projection;

% whichMonths = [8 9];
% yearRange = [2007 2017];
% varsToGrab = {'z@500','z@850','mslp','blh','eis','blh-ew-grad','wind-vec@900','wind-vec@950','wind-vec@1000'};
% save_era5_vars_for_projection;

% whichMonths = [10 11 12];
% yearRange = [2007 2017];
% varsToGrab = {'z@500','z@850','mslp','blh','eis','blh-ew-grad','wind-vec@900','wind-vec@950','wind-vec@1000'};
% save_era5_vars_for_projection;

clearvars -except region
init;
whichHour = 22;

% whichMonths = [5];
% yearRange = [2007 2017];
% varsToGrab = {'sst','z@500','z@850','z@900','z@950','z@1000','mslp','blh','blh-ew-grad','rh@700','rh@750','rh@800',...
% 	'wind-vec@900','wind-vec@950','wind-vec@1000','u@900','u@950','u@1000','v@900','v@950','v@1000','eis','lts'};
% save_era5_vars_for_projection;

% whichMonths = [1 2];
% yearRange = [2008 2017];
% varsToGrab = {'sst','z@500','z@850','z@900','z@950','z@1000','mslp','blh','blh-ew-grad','rh@700','rh@750','rh@800',...
% 	'wind-vec@900','wind-vec@950','wind-vec@1000','u@900','u@950','u@1000','v@900','v@950','v@1000','eis','lts'};
% save_era5_vars_for_projection;

% whichMonths = [4 5 6];
% yearRange = [2007 2017];
% varsToGrab = {'sst','z@500','z@850','z@900','z@950','z@1000','mslp','blh','blh-ew-grad','rh@700','rh@750','rh@800',...
% 	'wind-vec@900','wind-vec@950','wind-vec@1000','u@900','u@950','u@1000','v@900','v@950','v@1000','eis','lts'};
% save_era5_vars_for_projection;

whichMonths = [8 9];
yearRange = [2007 2017];
varsToGrab = {'rh@700','rh@750','rh@800'};
save_era5_vars_for_projection;

whichMonths = [10 11 12];
yearRange = [2007 2017];
varsToGrab = {'rh@700','rh@750','rh@800'};
save_era5_vars_for_projection;

whichMonths = 5;
yearRange = [2007 2017];
varsToGrab = {'v@925', 'v@975', 'wind-vec@925', 'wind-vec@975'};
save_era5_vars_for_projection;

whichMonths = [4 5 6];
yearRange = [2007 2017];
varsToGrab = {'v@925', 'v@975', 'wind-vec@925', 'wind-vec@975'};
save_era5_vars_for_projection;

whichMonths = 1:12;
yearRange = [2008 2017];
varsToGrab = {'v@900', 'v@925', 'v@950', 'v@975', 'wind-vec@900', 'wind-vec@925', 'wind-vec@950', 'wind-vec@975'};
save_era5_vars_for_projection;

clear
region = 'SEP';
init;
whichHour = 16;

whichMonths = [5];
yearRange = [2007 2017];
varsToGrab = {'blh-ew-grad',...
	'wind-vec@900','wind-vec@950','wind-vec@1000'};
save_era5_vars_for_projection;

whichMonths = [1 2];
yearRange = [2008 2017];
varsToGrab = {'blh-ew-grad',...
	'wind-vec@900','wind-vec@950','wind-vec@1000'};
save_era5_vars_for_projection;

whichMonths = [4 5 6];
yearRange = [2007 2017];
varsToGrab = {'blh-ew-grad',...
	'wind-vec@900','wind-vec@950','wind-vec@1000'};
save_era5_vars_for_projection;

whichMonths = [8 9];
yearRange = [2007 2017];
varsToGrab = {'blh-ew-grad',...
	'wind-vec@900','wind-vec@950','wind-vec@1000'};
save_era5_vars_for_projection;

whichMonths = [10 11 12];
yearRange = [2007 2017];
varsToGrab = {'blh-ew-grad',...
	'wind-vec@900','wind-vec@950','wind-vec@1000'};
save_era5_vars_for_projection;

clearvars -except region
init;
whichHour = 4;

whichMonths = [5];
yearRange = [2007 2017];
varsToGrab = {'blh-ew-grad',...
	'wind-vec@900','wind-vec@950','wind-vec@1000'};
save_era5_vars_for_projection;

whichMonths = [1 2];
yearRange = [2008 2017];
varsToGrab = {'blh-ew-grad',...
	'wind-vec@900','wind-vec@950','wind-vec@1000'};
save_era5_vars_for_projection;

whichMonths = [4 5 6];
yearRange = [2007 2017];
varsToGrab = {'blh-ew-grad',...
	'wind-vec@900','wind-vec@950','wind-vec@1000'};
save_era5_vars_for_projection;

whichMonths = [8 9];
yearRange = [2007 2017];
varsToGrab = {'blh-ew-grad',...
	'wind-vec@900','wind-vec@950','wind-vec@1000'};
save_era5_vars_for_projection;

whichMonths = [10 11 12];
yearRange = [2007 2017];
varsToGrab = {'blh-ew-grad',...
	'wind-vec@900','wind-vec@950','wind-vec@1000'};
save_era5_vars_for_projection;



%% 10 Nov 2019 for 975-mb divergence
init;
plData.folder = [plData.folder 'divergence/'];
whichHour = 22;
whichMonths = [4 5 6];
yearRange = [2007 2017];
varsToGrab = {'div@975'};
save_era5_vars_for_projection;
whichMonths = [1 2];
yearRange = [2007 2017];
save_era5_vars_for_projection;
whichMonths = [5];
yearRange = [2007 2017];
save_era5_vars_for_projection;
