varname = 'mslp';
hourStr = '22Z';
varStr = 'MSLP';
monthStr = 'Jan-Feb';

%% code to do analysis of different SOM dimensions
% Yields 3 key variables
%   allSomDims: list of som dimensions (nx2 array)
%   allQe:      list of quantization errors (nx1 array)
%   allTe:      list of topographic errors (nx1 array)
% And makes a sammon projection plot for visual analysis
baseFilemask = sprintf('som_*_base_SEA_%s_%s_%s_2008-2017.mat', ...
    hourStr, varname, monthStr);

% figure
f = dir(['data/som_dim_analysis/' baseFilemask]);
numFiles = length(f);
allSomDims = nan(numFiles,2);
allQe = nan(numFiles,1);
allTe = nan(numFiles,1);

for iFile = 1 : numFiles
    
    % assume that the data folder is in the path
    load(['data/som_dim_analysis/' f(iFile).name])
    
    allSomDims(iFile,:) = somDims;
    
    [qe,te] = som_quality(sMap,base);
    allQe(iFile) = qe;
    allTe(iFile) = te;
    
%     P = sammon(sMap,2,10^-5,'errchange');
%     som_grid(sMap,'Coord',P)
%     print(gcf,sprintf('results/som_dim_analysis/sammon_%ix%i_%s_May_2007-2017.png', ...
%         somDims(1), somDims(2), baseVarname),'-dpng')
        
end

%% plot quantization & topographic error vs. # of nodes

numNodes = allSomDims(:,1).*allSomDims(:,2);

figure
scatter(numNodes,allQe,'r','square','LineWidth',1.75)
yyaxis right
scatter(numNodes,allTe,'b','square','LineWidth',1.75)
ylabel('Topographic Error')
yyaxis left
ylabel('Quantization Error')
xlabel('# of nodes')
title(['SOM errors for ' hourStr 'Z ' varStr ' in ' monthStr ' (2007-2017)'])
box on