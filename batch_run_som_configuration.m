%% Caller for run_som_configuration.m
% 
% REQUIRED VARIABLES (see run_SEA_soms.m for example usage)
%   somsToMake: 2D cell array
%   region:     character array
%
% OPTIONAL VARIABLES
%   somDims:    2-element vector
%   settings:   struct of various settings
%       .MaxWorkers = number of jobs allowed to be running and/or queued
%       .WaitUntilNextEnqueue = cell array. first element is minutes to wait after successful enqueue.
%           second element is a cell array of which SOM vars the program should wait after.
%           e.g. settings.WaitUntilNextEnqueue = {6, {'eis','lts'}};
%               -> would only wait 6 minutes after queueing a SOM running on 'eis' or 'lts'
%   

numSomsToMake = 0;
for i = 1 : size(somsToMake,1)
    numSomsToMake = numSomsToMake + length(somsToMake{i,5}) * length(somsToMake{i,3});
end

%% SOMs that can be created by run_som_configuration.m

% The first column of somsToMake must be variable names
% which match the ones in somsAvailable.
somsAvailable = get_available_som_vars();

%% Set default dimensions of SOMs if not defined or incorrect
if ~exist('somDims', 'var') || size(somDims,2) ~= 2
    somDims = [5 5];
    fprintf('Selected default SOM dimensions (%i x %i)\n', somDims(1), somDims(2));
end

%% default settings
c = parcluster;
if ~exist('settings', 'var')
    settings = struct();
end
if ~isfield(settings, 'MaxWorkers')
    settings.MaxWorkers = c.NumWorkers;
end
if isfield(settings, 'WaitUntilNextEnqueue')
    % used to pause queueing for WaitUntilNextEnqueue minutes...
    % make sure matches expected format
    w = settings.WaitUntilNextEnqueue;
    if ~iscell(w) || ~all(size(w) == [1 2]) || ~isnumeric(w{1}) || ~iscell(w{2}) || ~all(cellfun(@ischar,w{2}))
        error('settings.WaitUntilNextEnqueue provided but not in correct format. Exiting...');
    end
    clear w
end
% ensure setting enabled for running in batch mode
settings.runBatchMode = 1;

%% Run configurations

if size(somDims,1) > 1
    batchLogFile = sprintf('logs/%s_batch_run_%s.txt', datestr(now,'yymmdd_HHMM'), region);
else
    batchLogFile = sprintf('logs/%s_batch_run_%ix%i_%s.txt', datestr(now,'yymmdd_HHMM'), ...
        somDims(1), somDims(2), region);
end
fid = fopen(batchLogFile, 'w');
if fid > 0
    fclose(fid);
    fprintf('Batch log file created!\n');
end
settings.batchLogFile = batchLogFile;

fprintf('Queueing SOM configurations...\n\n')

jobs = cell(0,1);
numQueued = 0;
numFinished = 0;

% loop through somsToMake
for i = 1 : size(somsToMake,1)

    if ~ismember(somsToMake{i,1}, somsAvailable)
        fprintf('Warning: variable "%s" not available for training. Skipping...\n', somsToMake{i,1});
        continue;
    end
    
    somVarId = find(ismember(somsAvailable, somsToMake{i,1}));
    if ~isempty(somsToMake{i,2})
        somDims = somsToMake{i,2};
    end
    hoursToUse = somsToMake{i,3};
    yearRange = somsToMake{i,4};
    monthsToUse = somsToMake{i,5};

    for j = 1 : size(somDims, 1)

        thisSomDim = somDims(j,:);

        % loop through each month(s) to run SOM on
        for m = monthsToUse

            somToMake = sprintf('%s, %s', somsAvailable{somVarId}, m{:});

            % check how many jobs are already running (don't add to queue when already alot of stuff running / on the queue)
            [~,queuedJobs,runningJobs,~] = findJob(c);
            while numel(runningJobs) >= min(settings.MaxWorkers, c.NumWorkers) ...
               || numel(queuedJobs) >= ceil(min(settings.MaxWorkers, c.NumWorkers) / 2)
                if length(jobs) == 0
                    % No jobs started yet, sleep until others are done
                    fprintf('\nWaiting for jobs elsewhere to finish...\n\n');
                    pause(round(rand*5*60) + 5*60); % sleeps a random amount of time between 5 and 10 minutes
                else
                    fprintf('Waiting for jobs to finish...\n');
                    wait(jobs{1}, 'finished');
                    numFinished = numFinished + 1;
                    fprintf('%i/%i jobs finished...\n', numFinished, numSomsToMake);
                    diary(jobs{1}, batchLogFile);
                    delete(jobs{1});
                    jobs(1) = [];
                end
                [~,queuedJobs,runningJobs,~] = findJob(c);
            end
            
            % loop through individual hours to run SOM on for each month(s) set
            for h = hoursToUse
                
                try
                    configVars = {region, somToMake, thisSomDim, yearRange, h, settings};
                    % Optionally add sfcData and/or plData to variables provided to run_som_configuration
                    %   - if no sfcData, will add {struct(), plData} to configVars
                    if exist('plData','var')
                        if exist('sfcData','var')
                            configVars{end+1} = sfcData;
                            configVars{end+1} = plData;
                        else
                            configVars{end+1} = struct();
                            configVars{end+1} = plData;
                        end
                    elseif exist('sfcData','var')
                        configVars{end+1} = sfcData;
                    end
                    jobs{end+1} = batch('run_som_configuration', 0, configVars);

                    numQueued = numQueued + 1;
                    fprintf('%i/%i configurations queued...\n', numQueued, numSomsToMake);

                    % if successfully enqueued, wait to do the next one
                    if isfield(settings, 'WaitUntilNextEnqueue')
                        w = settings.WaitUntilNextEnqueue;
                        [somVar,~] = parse_varname_level(somToMake);
                        if ismember(somVar, w{2})
                            fprintf('Waiting %i minutes to queue next configuration...\n', w{1});
                            pause(w{1} * 60);
                        end
                    end
                    
                catch e
                    fprintf('Error running %s SOM on %s: %s\n\n', somToMake, region, e.message);
                    % TODO make this stop the for-loop and wait for jobs to finish rather than just erroring out
                    rethrow(e)
                end
                
            end

        end

    end
    
end

fprintf('Queued all configurations!\n\n');

fprintf('Waiting for jobs to finish...\n\n');

numJobs = length(jobs);
for i = 1 : numJobs

    wait(jobs{i}, 'finished');
    fprintf('%i/%i jobs finished...\n', numFinished + i, numSomsToMake);
    diary(jobs{i}, batchLogFile);
    delete(jobs{i});

end

fprintf('\nFinished all configurations!\n\n');