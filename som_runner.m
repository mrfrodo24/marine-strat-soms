% Steps of running a SOM:
%   1) Load the base layer data to initialize the SOM
%   2) Convert base data to anomalies
%   3) Reshape the base data into 2D array 
%       - SOM accepts only 1D or 2D arrays. For our purposes, we give it 2D
%         data, where rows are the records and each column is a location.
%   4) Create the SOM (trains it automatically a few times)
%   5) Save the data for analysis

% SCRIPT EXECUTION NOTES
%   Required pre-functions
%       - define_constants
%       - user_params
%       - init (at least once)
%
%   Can optionally provide struct variable of processing parameters.
%       variable name must be 'settings'
%
%   Optional 'settings' parameters:
%       skipInit - Set to 1 to skip the initialization
%       runWithFullValues - Train on full values instead of normalized anomalies
%       

%% get settings
if ~exist('settings', 'var')
    settings = struct();
end

%% initialization
% user settings, define constants
if ~isfield(settings, 'skipInit') || ~settings.skipInit
    init;
end

%% load base data
% Load base data for the SOM. This will typically be some large-scale
% field, e.g. 500-mb geopotential height anomalies
%
% Right now this is only configured to work with just ERA5 netCDF data.
% See the load_som_base function for details.
if ~exist('base', 'var') || ~exist('t', 'var')
    [base, t] = load_som_base_fromsingle(datapath, ncfile, baseVarname, baseLevel);
end

if ~exist('lat', 'var') && ~exist('lon', 'var')
    lat = double(ncread(ncfilepath, LATITUDE));
    lon = double(ncread(ncfilepath, LONGITUDE));
end
[meshLat,meshLon] = meshgrid(lat,lon);

% check if computing anomalies for some hour each day, or if computing
% anomalies for each day's average
if isempty(whichHours)
    % convert data to daily means
    [base,t] = compute_daily_mean(t, base);
else
    % filter data to specific hour
    hourFilter = ismember(hour(t), whichHours);
    t = t(hourFilter);
    base = base(:,:,hourFilter);

    if isempty(t)
        error('No data for specified hour(s).')
    end
end

if isfield(settings, 'runWithFullValues') && settings.runWithFullValues
    % update base filename to indicate using full values
    somBaseFilename = strrep(somBaseFilename, ['_' baseVarname '_'], ['_' baseVarname '-full_']);
else
    % make base data into anomalies
    base = compute_running_anomaly(t, base, lengthOfMean, 1, 1, meshLat);
    % ensure no NaNs or Infs
    base(isnan(base) | isinf(base)) = 0;
end

% only get data from specified month(s)
monthFilter = ismember(month(t), whichMonths);
t = t(monthFilter);
base = base(:,:,monthFilter);
if isempty(t)
    error('No data for specified month(s) and years.')
end

% additional time filter for boundary days
if isfield(settings, 'runOnYesDays') || isfield(settings, 'runOnNoDays')
    prep_boundaries_xref;
    if isfield(settings, 'runOnYesDays') && settings.runOnYesDays
        tFilter = ismember(d, yes);
    elseif isfield(settings, 'runOnNoDays') && settings.runOnNoDays
        tFilter = ismember(d, no);
    end
    t = t(tFilter);
    base = base(:,:,tFilter);
end

% reshape base data
szLon = size(lat, 1);
szLat = size(lat, 2);
base = reshape_3D_field_to_2D(base);


%% create the SOM with the base data

% Before starting the training, start logging command window output
warning('off','all');
if ~isfield(settings, 'runBatchMode') || ~settings.runBatchMode
    diaryFile = ['logs' filesep datestr(datenum(datetime),'yymmdd_HHMMSS_') ...
        somBaseFilename '_log.txt'];
    diary(diaryFile);
end

% Base data must be a 2D array at this point. 
sMap = som_make(base, 'rect', 'msize', somDims);
sMap0 = som_lininit(base);
fprintf('\n');

% Turn off logging
if ~isfield(settings, 'runBatchMode') || ~settings.runBatchMode
    diary('off');
end
warning('on','all');


%% Save off data
save(['data/' somBaseFilename])

