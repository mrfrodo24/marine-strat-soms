%% Caller for run_som_configuration.m
% 
% REQUIRED VARIABLES (see run_SEA_soms.m for example usage)
%   somsToMake: 2D cell array
%   region:     character array
%
% OPTIONAL VARIABLES
%   somDims:    2-element vector
%   

numSomsToMake = 0;
for i = 1 : size(somsToMake,1)
    numSomsToMake = numSomsToMake + length(somsToMake{i,4}) * length(somsToMake{i,2});
end

%% SOMs that can be created by run_som_configuration.m

% The first column of somsToMake must be variable names
% which match the ones in somsAvailable.
somsAvailable = get_available_som_vars();

%% Set default dimensions of SOMs if not defined or incorrect
if ~exist('somDims', 'var') || ~all(size(somDims) == [1 2])
    somDims = [5 5];
    fprintf('Selected default SOM dimensions (%i x %i)\n', somDims(1), somDims(2));
end

%% default settings
if ~exist('settings', 'var')
    settings = struct();
end

%% Run configurations
numFinished = 0;

fprintf('Running SOM configurations...\n\n')
for i = 1 : size(somsToMake,1)

    if ~ismember(somsToMake{i,1}, somsAvailable)
        fprintf('Warning: variable "%s" not available for training. Skipping...\n', somsToMake{i,1});
        continue;
    end
    
    somVarId = find(ismember(somsAvailable, somsToMake{i,1}));
    % TODO somDims can now be passed via somsToMake{i,2}, might want to add support for this later
    hoursToUse = somsToMake{i,3};
    yearRange = somsToMake{i,4};
    monthsToUse = somsToMake{i,5};

    for m = monthsToUse

        somToMake = sprintf('%s, %s', somsAvailable{somVarId}, m{:});
    
        for h = hoursToUse
            
            try run_som_configuration(region, somToMake, somDims, yearRange, h, settings);
            catch e
                fprintf('Error running %s SOM on %s: %s\n\n', somToMake, region, e.message);
                rethrow(e)
            end
            numFinished = numFinished + 1;
            fprintf('%i/%i configurations completed...\n\n', numFinished, numSomsToMake);
            
        end

    end
        
end

fprintf('\nFinished all configurations!\n');