function run_som_configuration( region, somToMake, somDims, yearRange, whichHours, settings, varargin ) %#ok<INUSL>
%RUN_SOM_CONFIGURATION Stores pre-canned SOM configurations. A
%configuration is a set of user_params and commands to run before calling
%the som_runner. This function is used by run*soms caller scripts (e.g.
%run_SEA_soms.m).
%
% INPUTS
%
%   region:     identifier string of region of data for SOM run. Should
%               match one of the regions in the first switch statement below.
%
%   somToMake:  identifier string of SOM configuration. Should match one of
%               the identifier strings in the second switch statement below.
%   
%   somDims:    the dimensions of SOM nodes, given as columns by rows.
%
%   yearRange:  range of years to use for training SOM. Used in som_runner.m
%
%   whichHours: hours to pull from data for training SOM. Typically only 
%               want a single hour, to avoid getting diurnal cycle in 
%               normalized anomalies.
%
%   settings:   struct of various settings for running SOM.
%
%   varargin:
%       (1) sfcData
%       (2) plData
%
% NO OUTPUTS
%

%% constants
MONTHSTR = {'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'};

%% data paths and first file - may need modification!
SEA_FOLDER = '/home/disk/ivanova2/spencerwork/thesis/data/ECMWF/SEA_largescale_5to35S/';
SEP_FOLDER = '/home/disk/ivanova2/spencerwork/thesis/data/ECMWF/SEP_largescale_5to35S/';

SEA_SFC_FOLDER = [SEA_FOLDER 'sfc/'];
SEA_PL_FOLDER =  [SEA_FOLDER 'pl/'];
SEP_SFC_FOLDER = [SEP_FOLDER 'sfc/'];
SEP_PL_FOLDER =  [SEP_FOLDER 'pl/'];

SEA_FILEMASK = 'era5_largescale_SEA_%s_*.nc';
SEP_FILEMASK = 'era5_largescale_SEP_%s_*.nc';

SEA_SFC_FILEMASK = sprintf(SEA_FILEMASK, 'sfc');
SEA_PL_FILEMASK =  sprintf(SEA_FILEMASK, 'levels');
SEP_SFC_FILEMASK = sprintf(SEP_FILEMASK, 'sfc');
SEP_PL_FILEMASK =  sprintf(SEP_FILEMASK, 'levels');

SFC_FIRSTFILE =  '200701';
PL_FIRSTFILE =   '200701';

%% region finder

if length(varargin) >= 1,   sfcData = varargin{1}; end
if length(varargin) >= 2,   plData = varargin{2};  end

switch region
    case 'SEA'
        % Southeast Atlantic data
        if ~exist('sfcData','var')
            sfcData = struct(...
                'folder',   SEA_SFC_FOLDER, ...
                'filemask', SEA_SFC_FILEMASK ...
            );
        end
        if ~exist('plData','var')
            plData = struct(...
                'folder',   SEA_PL_FOLDER, ...
                'filemask', SEA_PL_FILEMASK ...
            );
        end
    
    case 'SEP'
        % Southeast Pacific data
        if ~exist('sfcData','var')
            sfcData = struct(...
                'folder',   SEP_SFC_FOLDER, ...
                'filemask', SEP_SFC_FILEMASK ...
            );
        end
        if ~exist('plData','var')
            plData = struct(...
                'folder',   SEP_PL_FOLDER, ...
                'filemask', SEP_PL_FILEMASK ...
            );
        end
        
    otherwise
        error('Region identifier not in pre-defined list of regions.')
end

%% som configuration runner

try

    % Guessing that if whichHours is just a single hour and if it's greater
    % than 16Z, probably looking at evening to assess potential for boundary on
    % following day (note: only applies to SEA)
    if isscalar(whichHours) && whichHours > 16
        addDayForBoundaryComparison = 1;
    else
        addDayForBoundaryComparison = 0;
    end

    % Get rest of params that are agnostic of region and training variable
    baseline_som_params;

    % get varname and level (if applicable) from configuration identifier
    fullVarStr = somToMake(1:strfind(somToMake,',')-1);
    [baseVarname, baseLevel] = parse_varname_level(fullVarStr);

    % parse out months from the configuration identifier
    whichMonths = strsplit(strtrim(somToMake(strfind(somToMake,',')+1:end)),'-');
    whichMonths = find(ismember(MONTHSTR, whichMonths));
    
    % pad months to load with surrounding months if doing anomalies (so
    % that anomalies can be computed for full window of whichMonths)
    whichMonthsToLoad = whichMonths;
    for i = 1:length(whichMonths)
        prevMonth = whichMonths(i) - 1;
        nextMonth = whichMonths(i) + 1;
        if ~ismember(prevMonth, whichMonthsToLoad)
            whichMonthsToLoad = [prevMonth, whichMonthsToLoad]; %#ok<AGROW>
        end
        if ~ismember(nextMonth, whichMonthsToLoad)
            whichMonthsToLoad = [whichMonthsToLoad, nextMonth]; %#ok<AGROW>
        end
    end
    % usually sorted already but make sure
    whichMonthsToLoad = sort(whichMonthsToLoad);

    switch baseVarname

        case {'u', 'v', 'z'}
            % set ncfilepath to first file to load lats and lons
            ncfilepath = [plData.folder strrep(plData.filemask, '*', PL_FIRSTFILE)];
            [base,t] = load_som_base_bymonth(plData.folder, plData.filemask, baseVarname, baseLevel);

        case 'blh'
            % set ncfilepath to first file to load lats and lons
            ncfilepath = [sfcData.folder strrep(sfcData.filemask, '*', SFC_FIRSTFILE)];
            [base,t] = load_som_base_bymonth(sfcData.folder, sfcData.filemask, baseVarname, []);

        case {'rh', 'div'}
            if strcmp(baseVarname, 'rh'),   era5varname = 'r'; end
            if strcmp(baseVarname, 'div'),  era5varname = 'd'; end
            % set ncfilepath to first file to load lats and lons
            ncfilepath = [plData.folder strrep(plData.filemask, '*', PL_FIRSTFILE)];
            [base,t] = load_som_base_bymonth(plData.folder, plData.filemask, era5varname, baseLevel);

        case 'mslp'
            % set ncfilepath to first file to load lats and lons
            ncfilepath = [sfcData.folder strrep(sfcData.filemask, '*', SFC_FIRSTFILE)];
            [base,t] = load_som_base_bymonth(sfcData.folder, sfcData.filemask, 'msl', []);

        case 'sst'
            % set ncfilepath to first file to load lats and lons
            ncfilepath = [sfcData.folder strrep(sfcData.filemask, '*', SFC_FIRSTFILE)];
            [base,t] = load_era5_SST(sfcData);
            
        case 'lts'
            % set ncfilepath to first file to load lats and lons
            ncfilepath = [sfcData.folder strrep(sfcData.filemask, '*', SFC_FIRSTFILE)];
            [base,t] = load_era5_LTS(sfcData, plData);
            
        case 'eis'
            % set ncfilepath to first file to load lats and lons
            ncfilepath = [sfcData.folder strrep(sfcData.filemask, '*', SFC_FIRSTFILE)];
            [base,t] = load_era5_EIS(sfcData, plData, whichMonthsToLoad);
            
        case 'blh-rh'
            % set ncfilepath to first file to load lats and lons
            ncfilepath = [plData.folder strrep(plData.filemask, '*', PL_FIRSTFILE)];
            [base,t] = load_era5_BLH_from_RH_profiles(plData);

        case 'modis-peakLwc'
            % lat and lon picked up by som_runner
            [base,t,lat,lon] = load_modis_base(settings.modisMatFilePath, 'peakLwc'); %#ok<ASGLU>

        case 'modis-lwp'
            % lat and lon picked up by som_runner
            [base,t,lat,lon] = load_modis_base(settings.modisMatFilePath, 'lwp'); %#ok<ASGLU>
            
        otherwise
            error([somToMake ' not in pre-defined list of SOM configurations.'])
    end

    % Zero out all NaNs from base data since SOMs can't handle NaNs
    base(isnan(base)) = 0;

    % make sure ncfilepath is reading from corresponding pl file
    if baseLevel
        ncfilepath = strrep(ncfilepath, '_levels_', ['_' num2str(baseLevel) 'mb_']);
    end

    % ensure data is only for specified years
    yearFilter = year(t) >= yearRange(1) & year(t) <= yearRange(2);
    t = t(yearFilter);
    base = base(:,:,yearFilter);

    % define some strings to be used in plot titles and file names
    dayStr = '';
    if isfield(settings, 'runOnYesDays') && settings.runOnYesDays,   dayStr = 'Yes-Days ';
    elseif isfield(settings, 'runOnNoDays') && settings.runOnNoDays, dayStr = 'No-Days ';
    end
    monthStr = sprintf([repmat('%s-',1,length(whichMonths)-1) '%s'], MONTHSTR{whichMonths});
    hourStr      = strjoin(strsplit(num2str(whichHours)), '.');
    hourTitleStr = strjoin(strsplit(num2str(whichHours)), ',');
    if ~isempty(hourTitleStr), hourTitleStr = [hourTitleStr 'Z ']; end
    yearStr = sprintf('%i-%i', yearRange(1), yearRange(2));
    plotTitleSuffix = [' (' hourTitleStr dayStr 'in ' monthStr ', ' yearStr ')'];

    % set final configuration variables
    somBaseFilename = sprintf('%s_%s_%sZ_%s_%s_%s', baseFilePrefix, region, hourStr, fullVarStr, monthStr, yearStr);
    if isfield(settings, 'runOnYesDays') && settings.runOnYesDays
        somBaseFilename = strrep(somBaseFilename, yearStr, [yearStr '_yesdays']);
    elseif isfield(settings, 'runOnNoDays') && settings.runOnNoDays
        somBaseFilename = strrep(somBaseFilename, yearStr, [yearStr '_nodays']);
    end
    somBasePlotPrefix = sprintf('%s anomalies SOM', get_long_varname(fullVarStr));
    somProjectionTitleBase = sprintf('based on %s SOM', get_short_varname(baseVarname));
    somBasePlotTitle = [somBasePlotPrefix plotTitleSuffix];
    somProjectionTitleSuffix = [somProjectionTitleBase plotTitleSuffix];

    % don't need to re-run init, just define_constants
    define_constants;
    settings.skipInit = 1;

    % run the SOM
    som_runner;

catch e

    disp(e.message);
    if isfield(settings,'runBatchMode') && settings.runBatchMode
        for k = 1 : length(e.stack)
            e.stack(k)
        end
        diary(settings.batchLogFile);
    end

end

end

