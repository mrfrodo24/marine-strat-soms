
% Basic initialization steps for marine-strat-soms
initDirs = {...
    'data', ...
    'helpers', ...
    'io', ...
    'physics', ...
    'plotting', ...
    'settings', ...
    'statistics', ...
    'transformers' ...
};
for i = 1 : length(initDirs)
	if isempty(strfind(path, [pwd filesep initDirs{i}]))
		addpath(genpath(['./' initDirs{i}]));
	end
end

if ~exist('datapath','var') && ~exist('sfcData','var') && ~exist('plData','var')
    user_params;
end

if isempty(strfind(path, [marineStratToolsPath 'io']))
	addpath(genpath([marineStratToolsPath 'io']));
end
if isempty(strfind(path, [marineStratToolsPath 'utils']))
	addpath(genpath([marineStratToolsPath 'utils']));
end
if isempty(strfind(path, sompakPath))
	addpath(genpath(sompakPath));
end

define_constants;