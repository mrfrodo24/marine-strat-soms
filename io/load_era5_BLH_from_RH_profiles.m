function [ blh, d, varargout ] = load_era5_BLH_from_RH_profiles(plData, varargin)
%LOAD_ERA5_EIS Function to load / derive boundary layer height from
%profiles of relative humidity and geopotential height.
%   
% INPUTS:
%   plData - struct defining where pressure level data is and the mask
%             of data files to load
%       .folder = where files are located
%       .filemask = mask for files (uses a wildcard '*')
%   varargin
%       (1) whichMonthsToLoad - the months of data to load
%
% REQUIRED FIELDS FOR BLH:
%   - relative humidity from 1000-700 mb [%]
%   - geopotential height from 1000-700 mb [m]
%

whichMonthsToLoad = [];
if length(varargin) >= 1
    whichMonthsToLoad = varargin{1};
end
whichHoursToLoad = [];
if length(varargin) >= 2
    whichHoursToLoad = varargin{2};
end

% Find first and last month to process
files = dir([plData.folder plData.filemask]);
if isempty(files)
    error(['No files to load for ' plData.folder plData.filemask])
end

% Load the data, deriving BLH after loading each month
textprogressbar('Loading BLH using RH threshold... ', length(files));
counter = 1;
for i = 1:length(files)
    textprogressbar(i);
    iFile = files(i);
    [monthRh, t] = load_som_base_fromsingle(plData.folder, iFile.name, 'r', []);
    [monthZ, ~] = load_som_base_fromsingle(plData.folder, iFile.name, 'z', []);
    if ~isempty(whichMonthsToLoad)
        t(~ismember(month(t), whichMonthsToLoad)) = [];
        if isempty(t)
            continue;
        end
    end
    if ~isempty(whichHoursToLoad)
        hourFilter = ~ismember(hour(t), whichHoursToLoad);
        monthRh(:,:,:,hourFilter) = [];
        monthZ(:,:,:,hourFilter) = [];
        t(hourFilter) = [];
        if isempty(t)
            continue;
        end
    end
    
    monthBlh = estimate_blh_from_rh_thresh_mex(monthRh, monthZ);
    
    arraySize = size(monthRh);
    timeSize = length(t);
    
    if ~exist('blh','var')
        fullTimeSize = length(files) * timeSize;
        z = nan([arraySize(1:3) fullTimeSize]);
        rh = nan([arraySize(1:3) fullTimeSize]);
        blh = nan([arraySize([1 2]), fullTimeSize]);
        d = nan(fullTimeSize, 1);
    end
    thisTimeRange = counter : counter+timeSize-1;
    blh(:,:,thisTimeRange) = monthBlh;
    rh(:,:,:,thisTimeRange) = monthRh;
    z(:,:,:,thisTimeRange) = monthZ;
    d(thisTimeRange) = t;
    
    counter = counter + timeSize;
end

textprogressbar(' done!');

blh(:,:,isnan(d)) = [];
rh(:,:,:,isnan(d)) = [];
z(:,:,:,isnan(d)) = [];
d(isnan(d)) = [];
varargout{1} = rh;
varargout{2} = z;

end

