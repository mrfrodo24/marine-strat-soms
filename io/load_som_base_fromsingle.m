function [base_variable, t] = load_som_base_fromsingle(datapath, ncfile, var, level)
%LOAD_SOM_BASE Load the baseline data for a SOM analysis
%
%   INPUTS:
%       datapath
%       ncfile
%       var - name of variable in netCDF file to load
%       level - if applicable, provide level to load. if not applicable,
%           set to [] (empty array).
%

if ~isempty(datapath) && datapath(end) ~= '/' && datapath(end) ~= '\'
    filepath = [datapath filesep ncfile];
else
    filepath = [datapath ncfile];
end
t = datenum(1900,1,1, double(ncread(filepath,'time')), 0,0);

%% Read data

if ~isempty(level)
    filepath = strrep(filepath, '_levels_', ['_' num2str(level) 'mb_']);
    % check if nc file even has a level dimension, if it was directly downloaded
    % with just one level then it won't have it. if it was hyperslabbed, it will
    % still have the level dimension but will be just the 1 value.
    finfo = ncinfo(filepath);
    if ismember('level', {finfo.Dimensions.Name})
        levels = double(ncread(filepath, 'level'));
        il = find(levels == level,1,'first');
        data = double(ncread(filepath, var));
        data = squeeze(data(:,:,il,:));
    else
        data = double(ncread(filepath, var));
    end
else
    data = double(ncread(filepath, var));
end
base_variable = unitConversion(var, data);

% Used these in testing, don't need right now
% lonSize = size(geop, 1);
% latSize = size(geop, 2);
% numRecords = size(geop, 4);

clear data;


function [ data ] = unitConversion(type, data)

    if ismember(type, {'pa-to-hpa', 'Pa-to-hPa', 'msl', 'sp'})
        data = data ./ 100; % Pa to hPa
    elseif ismember(type, {'QV2M', '2_meter_specific_humidity'})
        data = data .* 1000; % kg/kg to g/kg
    elseif ismember(type, {'z', 'geopotential'})
        data = data ./ 9.81; % m^2/s^2 to m
    end

end

end

