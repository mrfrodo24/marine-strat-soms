function [base,t] = load_som_base(folder, files, var, level, varargin)
%LOAD_SOM_BASE Load data for SOM from ERA5, but call appropriate loader
%based on whether loading from monthly netCDF files or one global file.

if ~isempty(strfind(files,'*'))
    [base,t] = load_som_base_bymonth(folder, files, var, level, varargin{:});
else
    [base,t] = load_som_base_fromsingle(folder, files, var, level);
end

end

