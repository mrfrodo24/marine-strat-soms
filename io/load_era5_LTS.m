function [ LTS, t, varargout ] = load_era5_LTS(sfcData, plData, varargin)
%LOAD_ERA5_LTS Function to load LTS from ERA5 monthly data files.
%   
%   INPUTS:
%       sfcData - struct defining where surface data is and the mask
%                   of data files to load
%       plData - struct defining where pressure level data is and the mask
%                   of data files to load
%           .folder = where files are located
%           .filemask = mask for files (uses a wildcard '*')
%       varargin - if already loaded pre-req data, pass through here (must
%               pass all for it to work)
%           (1) time (Matlab datenums)
%           (2) temp @ sfc in K
%           (3) temp @ 700 mb in K
%
%   OUTPUTS:
%       LTS - lower-tropospheric stability in K
%       t - time as Matlab datenums
%       varargout - 
%           (1) theta @ sfc in K
%           (2) theta @ 700 mb in K
%

argsSfc = {};
args700mb = {};
if ~isempty(varargin)
    argsSfc = varargin([1 2]);
    args700mb = varargin([1 3]);
end
[theta0, t] = load_era5_potential_temperature(sfcData.folder, sfcData.filemask, [], argsSfc{:});

[theta700, ~] = load_era5_potential_temperature(plData.folder, plData.filemask, 700, args700mb{:});

LTS = compute_LTS(theta0, theta700);

varargout{1} = theta0;
varargout{2} = theta700;

end

