function [theta, t] = load_era5_potential_temperature(datapath, ncfile, level, varargin)
%LOAD_ERA5_POTENTIAL_TEMPERATURE
%
%   INPUTS:
%       datapath
%       ncfile - if contains an asterisk (*), assumes this is a file mask
%           and need to read data in monthly files.
%       level - pressure level (or [] if surface)
%       varargin: if already loaded some of the pre-req data, pass through
%               here (must provide both or none)
%           t - times (Matlab datenums)
%           temp - temperature at specified level or at sfc

if length(varargin) == 2
    t = varargin{1};
    temp = varargin{2};
else
    if isempty(level)
        [temp, t] = load_som_base(datapath, ncfile, 't2m', []);
    else
        [temp, t] = load_som_base(datapath, ncfile, 't', level);
    end
end

% Get surface pressure if doing potential temperature at surface
if isempty(level)
    [p, tp] = load_som_base(datapath, ncfile, 'sp', []);
    p(:,:,~ismember(tp,t)) = [];
    tp(~ismember(tp,t)) = [];
else
    p = level;
end

p0 = 1000; % standard pressure (mb)
R = 287.058; % gas constant of dry air (J/kg/K)
cp = 1005.7; % specific heat of dry air (J/kg/K)

theta = temp .* (p0 ./ p).^(R/cp);

end