function [ EIS, t ] = load_era5_EIS(sfcData, plData, varargin)
%LOAD_ERA5_EIS Function to load EIS from ERA5 monthly data files.
%   
% INPUTS:
%   sfcData - struct defining where surface data is and the mask
%             of data files to load
%   plData - struct defining where pressure level data is and the mask
%             of data files to load
%       .folder = where files are located
%       .filemask = mask for files (uses a wildcard '*')
%   varargin
%       (1) whichMonthsToLoad - the months of data to load
%
% REQUIRED FIELDS FOR EIS:
%   - sfc temperature [K]
%   - LCL [m] => sfc temperature and dew point
%   - LTS [K] => potential temperature @ sfc and 700 mb
%   - temperature @ 700 mb [K]
%   - geopotential height @ 700 mb [m]
%

% Load all necessary data
[temp2m,t] 	= load_som_base(sfcData.folder, sfcData.filemask, 't2m', [], varargin{:});
[dewp2m,~] 	= load_som_base(sfcData.folder, sfcData.filemask, 'd2m', [], varargin{:});
[temp700,~] = load_som_base(plData.folder, plData.filemask, 't', 700, varargin{:});
[z700,~] 	= load_som_base(plData.folder, plData.filemask, 'z', 700, varargin{:});
[LTS,~]     = load_era5_LTS(sfcData, plData, t, temp2m, temp700);

% Calculate the LCL
LCL = compute_LCL(temp2m, dewp2m);

% Run calculation for EIS
EIS = compute_EIS(LTS, temp2m, temp700, z700, LCL);

end

