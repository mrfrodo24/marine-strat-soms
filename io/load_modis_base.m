function [base,t,lat,lon] = load_modis_base(pathToMatFile, var)
%LOAD_SOM_BASE Load data for SOM from MODIS. Uses mat files produed with
%the marine-strat-tools package.
%
%	MAT-file has the following expected variables, based on 
%	marine-strat-tools/runners/modis_lwc_thickness_seasonal.m
%		- var (the specified variables, e.g. peakLwc)
%		- dates
%		- xdim
%		- ydim
%		- minLon
%		- maxLon
%		- minLat
%	    - maxLat

%% constants
% these are hard-coded for now
variablesToLoad = { ...
	var, ...
	'dates', ...
	'xdim', ...
	'ydim', ...
	'minLon', ...
	'maxLon', ...
	'minLat', ...
	'maxLat' ...
};
maxSubsetLat = -5;
minSubsetLat = -35;
minSubsetLon = -10;
maxSubsetLon = 20;

%% load data
load(pathToMatFile, variablesToLoad{:})
xdim = double(xdim);
ydim = double(ydim);
t = dates';

%% subset data
lat = ydim(ydim >= minLat & ydim <= maxLat);
lon = xdim(xdim >= minLon & xdim <= maxLon);
subsetLatFilt = lat >= minSubsetLat & lat <= maxSubsetLat;
subsetLonFilt = lon >= minSubsetLon & lon <= maxSubsetLon;

base = [];
eval(sprintf('base = %s(subsetLatFilt, subsetLonFilt, :);', var));
lat = lat(subsetLatFilt);
lon = lon(subsetLonFilt);

%% Set NaNs to zero
base(isnan(base)) = 0;

%% Transpose MODIS array because it's lat x lon
base = permute(base,[2 1 3]);

end
