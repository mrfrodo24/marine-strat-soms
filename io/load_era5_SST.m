function [ SST, t, varargout ] = load_era5_SST(sfcData, varargin)
%LOAD_ERA5_SST Function to load SSTs from ERA5 monthly data files.
%
% Since sea-surface temperatures are not present over land, need
% to reduce array down to only contain non-nan values (over oceans).
%   
% INPUTS
%   sfcData - struct defining where surface data is and the mask
%             of data files to load
%	varargin
%		(1) if provided, whether to fill SST NaNs with 2-meter temperature.
%
% OUTPUTS
%	varargout
%		(1) - longitudes (nx1 vector)
%		(2) - latitudes (mx1 vector)
%       

%THIS STEP NO LONGER NEEDED (filling NaN SST data with T2M)
% Load latitudes and longitudes
% listOfFiles = dir([sfcData.folder sfcData.filemask]);
% firstFilePath = [sfcData.folder listOfFiles(1).name];
% lat = double(ncread(firstFilePath, 'latitude'));
% lon = double(ncread(firstFilePath, 'longitude'));

% Load sst data
[fullsst, t] = load_som_base_bymonth(sfcData.folder, sfcData.filemask, 'sst', []);

if isempty(varargin) || varargin{1}
	[t2m, ~] = load_som_base_bymonth(sfcData.folder, sfcData.filemask, 't2m', []);

	if ~all(size(fullsst) == size(t2m))
		error('SST and 2-meter temp. fields are not the same size, cannot combine. Exiting.')
	end
	disp('Filling SST NaNs over land with 2-meter temperature...')
	fullsst(isnan(fullsst)) = t2m(isnan(fullsst));
end

% make sure no NaNs in times...
SST = fullsst(:,:,~isnan(t));
t = t(~isnan(t));

%THIS STEP NO LONGER NEEDED (filling NaN SST data with T2M)
% Reduce matrix so it only contains non-nan values
% [SST, lon, lat] = get_inner_nonnan_rect(fullsst, lon, lat);
% varargout{1} = lon;
% varargout{2} = lat;

end

