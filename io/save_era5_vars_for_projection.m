% Load various data and save off subsets for projections

%% user params
% region = 'SEP';
% whichHour = 22;
% varsToGrab = {'eis','lts','z@500','z@850','mslp','blh','blh-ew-grad','rh@700','rh@750','rh@800',...
% 	'wind-vec@900','wind-vec@950','wind-vec@1000','u@900','u@950','u@1000','v@900','v@950','v@1000'};
% whichMonths = [5];
% yearRange = [2007 2017];

%% constants
ALL_MONTHS = {'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'};

%% main
monthStr = '';
for i = 1 : length(whichMonths)-1
	monthStr = [monthStr ALL_MONTHS{whichMonths(i)} '-'];
end
monthStr = [monthStr ALL_MONTHS{whichMonths(end)}];

for i = 1 : length(varsToGrab)
	
	varToGrab = varsToGrab{i}
	idOfAtSymbol = strfind(varToGrab,'@');
	if ~isempty(idOfAtSymbol)
		level = str2num(varToGrab(idOfAtSymbol+1 : end));
		varToGrab = varToGrab(1 : idOfAtSymbol-1);
	end
	varsToSave = {'tp','varToProject'};

	switch varToGrab

	case 'eis'
		[varToProject,tp] = load_era5_EIS(sfcData, plData, whichMonths);

	case 'lts'
		[varToProject,tp] = load_era5_LTS(sfcData, plData);

	case 'sst'
		[varToProject,tp] = load_era5_SST(sfcData);

	case {'u','v','z'}
		[varToProject,tp] = load_som_base_bymonth(plData.folder, plData.filemask, varToGrab, level);

	case 'rh'
		[varToProject,tp] = load_som_base_bymonth(plData.folder, plData.filemask, 'r', level);

	case 'div'
		[varToProject,tp] = load_som_base_bymonth(plData.folder, plData.filemask, 'd', level);

	case 'mslp'
		[varToProject,tp] = load_som_base_bymonth(sfcData.folder, sfcData.filemask, 'msl', []);

	case 'blh'
		[varToProject,tp] = load_som_base_bymonth(sfcData.folder, sfcData.filemask, 'blh', []);

	case 'blh-rh'
		[varToProject,tp] = load_era5_BLH_from_RH_profiles(plData, whichMonths);

	case 'blh-ew-grad'
		listOfFiles = dir([sfcData.folder sfcData.filemask]);
		ncfilepath = [sfcData.folder listOfFiles(1).name];
		lat = double(ncread(ncfilepath, LATITUDE));
		lon = double(ncread(ncfilepath, LONGITUDE));
		[meshLat, meshLon] = meshgrid(lat, lon);
		[blh,tp] = load_som_base_bymonth(sfcData.folder, sfcData.filemask, 'blh', []);
		% compute E-W gradient of BLH
		kmPerDegLon = 2 * pi * cosd(meshLat(1:end-1,:)) * 6371 ./ 360;
		varToProject = (blh(2:end,:,:) - blh(1:end-1,:,:)) ./ ...
			repmat((meshLon(2:end,:) - meshLon(1:end-1,:)) .* kmPerDegLon, 1, 1, size(blh,3));
		% E-W gradient is in units of m/km
		lon = nanmean([lon(1:end-1) lon(2:end)], 2);

	case 'wind-vec'
		[uToProject,tp] = load_som_base_bymonth(plData.folder, plData.filemask, 'u', level);
		[vToProject,tp] = load_som_base_bymonth(plData.folder, plData.filemask, 'v', level);
		varsToSave = {'tp','uToProject','vToProject'};

	otherwise
		fprintf('Not a valid var to grab: %s\n', varToGrab);
		continue;
	end

	fprintf('Subsetting and saving data...');

	tFilter = year(tp) >= yearRange(1) & year(tp) <= yearRange(2) ...
		& ismember(month(tp), whichMonths) ...
		& ismember(hour(tp), whichHour) ...
		& ~isnan(tp);

	% variables to be saved should either be tp or a 3-dimensional variable to project
	for v = varsToSave
		switch v{1}
		case 'tp'
			tp = tp(tFilter);
		otherwise
			eval(sprintf('%s = %s(:,:,tFilter);', v{1}, v{1}));
		end
	end

	outFile = sprintf('data/varToProject_%s_%iZ_%s_%s_%i-%i.mat', region, whichHour, varsToGrab{i}, monthStr, yearRange(1), yearRange(2));

	% add lat and lon to outFile if exist
	if exist('lat','var'), varsToSave{end+1} = 'lat'; end
	if exist('lon','var'), varsToSave{end+1} = 'lon'; end

	save(outFile, varsToSave{:});
	clear lat lon; % in case they do exist

	fprintf('done!\n\n');

end

