function [allVarData, allTimes] = load_som_base_bymonth(datapath, filemask, var, level, varargin)
%LOAD_SOM_BASE Load the baseline data for a SOM analysis
%
% INPUTS:
%   datapath
%   ncfile
%   var:     name of variable in netCDF file to load
%   level:   if applicable, provide level to load. if not applicable,
%            set to [] (empty array).
%   varargin:
%       (1) whichMonths - the months of data to load
%
% OUTPUTS
%   allVarData: 3D array, where dim 1 is longitude, dim 2 is lats, and dim 3 is time
%   allTImes:   datenums corresponding to each time record in allVarData
%

whichMonths = [];
if length(varargin) >= 1, whichMonths = varargin{1}; end

allVarData = [];
allTimes = [];
if datapath(end) ~= '/' && datapath(end) ~= '\'
    datapath = [datapath filesep];
end

% If loading data on a pressure level, replace _levels_ in filemask with level
if ~isempty(level)
    filemask = strrep(filemask, '_levels_', ['_' num2str(level) 'mb_']);
end

% Loop through files identified by pathmask
files = dir([datapath filemask]);

loadingMsg = ['Loading ' var ' for SOM by month... '];
if ~isempty(level)
    loadingMsg = ['Loading ' var '@' num2str(level) ' for SOM by month... '];
end
textprogressbar(loadingMsg, length(files));
counter = 1;
for i = 1:length(files)
    textprogressbar(i);
    iFile = files(i);
    [varData, t] = load_som_base_fromsingle(datapath, iFile.name, var, level);
    if ~isempty(whichMonths)
        t(~ismember(month(t), whichMonths)) = [];
        if isempty(t)
            continue;
        end
    end
    d3 = size(varData,3);
    if isempty(allVarData)
        d1 = size(varData,1);
        d2 = size(varData,2);
        allVarData = nan(d1,d2,length(files)*d3);
        allTimes = squeeze(allVarData(1,1,:));
    end
    allVarData(:,:,counter:counter+d3-1) = varData;
    allTimes(counter:counter+d3-1) = t;
    counter = counter + d3;
end

allVarData(:,:,isnan(allTimes)) = [];
allTimes(isnan(allTimes)) = [];

textprogressbar(' done!');


end

