switch somRegion
case 'SEA'
    if hourToPlot == 22
        %% SEA 22Z
        somsForAnalysis = {...
            'u@900',    [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'u@900',    [22], 'May',            [2007 2017], 0, 0; ...
            'u@900',    [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'u@950',    [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'u@950',    [22], 'May',            [2007 2017], 0, 0; ...
            'u@950',    [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'u@1000',   [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'u@1000',   [22], 'May',            [2007 2017], 0, 0; ...
            'u@1000',   [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'v@900',    [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'v@900',    [22], 'May',            [2007 2017], 0, 0; ...
            'v@900',    [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'v@950',    [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'v@950',    [22], 'May',            [2007 2017], 0, 0; ...
            'v@950',    [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'v@1000',   [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'v@1000',   [22], 'May',            [2007 2017], 0, 0; ...
            'v@1000',   [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@900',    [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'z@900',    [22], 'May',            [2007 2017], 0, 0; ...
            'z@900',    [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@950',    [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'z@950',    [22], 'May',            [2007 2017], 0, 0; ...
            'z@950',    [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@1000',   [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'z@1000',   [22], 'May',            [2007 2017], 0, 0; ...
            'z@1000',   [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@500',    [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'z@500',    [22], 'May',            [2007 2017], 0, 0; ...
            'z@500',    [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@500',    [22], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'z@500',    [22], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'z@850',    [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'z@850',    [22], 'May',            [2007 2017], 0, 0; ...
            'z@850',    [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@850',    [22], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'z@850',    [22], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'rh@700',   [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'rh@700',   [22], 'May',            [2007 2017], 0, 0; ...
            'rh@700',   [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'rh@700',   [22], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'rh@700',   [22], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'rh@750',   [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'rh@750',   [22], 'May',            [2007 2017], 0, 0; ...
            'rh@750',   [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'rh@750',   [22], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'rh@750',   [22], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'rh@800',   [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'rh@800',   [22], 'May',            [2007 2017], 0, 0; ...
            'rh@800',   [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'rh@800',   [22], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'rh@800',   [22], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'mslp',     [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'mslp',     [22], 'May',            [2007 2017], 0, 0; ...
            'mslp',     [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'mslp',     [22], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'mslp',     [22], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'eis',      [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'eis',      [22], 'May',            [2007 2017], 0, 0; ...
            'eis',      [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'eis',      [22], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'eis',      [22], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'blh',      [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'blh',      [22], 'May',            [2007 2017], 0, 0; ...
            'blh',      [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'blh',      [22], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'blh',      [22], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ... % missed
            'sst',      [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'sst',      [22], 'May',            [2007 2017], 0, 0; ...
            'sst',      [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'sst',      [22], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'sst',      [22], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'lts',      [22], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'lts',      [22], 'May',            [2007 2017], 0, 0; ...
            'lts',      [22], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'lts',      [22], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'lts',      [22], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
        };

    elseif hourToPlot == 10
        %% SEA 10Z
        somsForAnalysis = {...
            'u@900',    [10], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'u@900',    [10], 'May',            [2007 2017], 0, 0; ...
            'u@900',    [10], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'u@950',    [10], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'u@950',    [10], 'May',            [2007 2017], 0, 0; ...
            'u@950',    [10], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'u@1000',   [10], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'u@1000',   [10], 'May',            [2007 2017], 0, 0; ...
            'u@1000',   [10], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'v@900',    [10], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'v@900',    [10], 'May',            [2007 2017], 0, 0; ...
            'v@900',    [10], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'v@950',    [10], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'v@950',    [10], 'May',            [2007 2017], 0, 0; ...
            'v@950',    [10], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'v@1000',   [10], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'v@1000',   [10], 'May',            [2007 2017], 0, 0; ...
            'v@1000',   [10], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@900',    [10], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'z@900',    [10], 'May',            [2007 2017], 0, 0; ...
            'z@900',    [10], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@950',    [10], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'z@950',    [10], 'May',            [2007 2017], 0, 0; ...
            'z@950',    [10], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@1000',   [10], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'z@1000',   [10], 'May',            [2007 2017], 0, 0; ...
            'z@1000',   [10], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@500',    [10], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'z@500',    [10], 'May',            [2007 2017], 0, 0; ...
            'z@500',    [10], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@500',    [10], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'z@500',    [10], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'z@850',    [10], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'z@850',    [10], 'May',            [2007 2017], 0, 0; ...
            'z@850',    [10], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@850',    [10], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'z@850',    [10], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'mslp',     [10], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'mslp',     [10], 'May',            [2007 2017], 0, 0; ...
            'mslp',     [10], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'mslp',     [10], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'mslp',     [10], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'eis',      [10], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'eis',      [10], 'May',            [2007 2017], 0, 0; ...
            'eis',      [10], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'eis',      [10], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'eis',      [10], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'blh',      [10], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'blh',      [10], 'May',            [2007 2017], 0, 0; ...
            'blh',      [10], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'blh',      [10], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'blh',      [10], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
        };
    end

case 'SEP'
    if hourToPlot == 4
        %% SEP 04Z
        somsForAnalysis = {...
            'z@500',    [4], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'z@500',    [4], 'May',            [2007 2017], 0, 0; ...
            'z@500',    [4], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@500',    [4], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'z@500',    [4], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'z@850',    [4], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'z@850',    [4], 'May',            [2007 2017], 0, 0; ...
            'z@850',    [4], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@850',    [4], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'z@850',    [4], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'rh@700',   [4], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'rh@700',   [4], 'May',            [2007 2017], 0, 0; ...
            'rh@700',   [4], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'rh@700',   [4], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'rh@700',   [4], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'rh@750',   [4], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'rh@750',   [4], 'May',            [2007 2017], 0, 0; ...
            'rh@750',   [4], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'rh@750',   [4], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'rh@750',   [4], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'rh@800',   [4], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'rh@800',   [4], 'May',            [2007 2017], 0, 0; ...
            'rh@800',   [4], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'rh@800',   [4], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'rh@800',   [4], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'mslp',     [4], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'mslp',     [4], 'May',            [2007 2017], 0, 0; ...
            'mslp',     [4], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'mslp',     [4], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'mslp',     [4], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'eis',      [4], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'eis',      [4], 'May',            [2007 2017], 0, 0; ...
            'eis',      [4], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'eis',      [4], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'eis',      [4], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'blh',      [4], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'blh',      [4], 'May',            [2007 2017], 0, 0; ...
            'blh',      [4], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'blh',      [4], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'blh',      [4], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'sst',      [4], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'sst',      [4], 'May',            [2007 2017], 0, 0; ...
            'sst',      [4], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'sst',      [4], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'sst',      [4], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'lts',      [4], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'lts',      [4], 'May',            [2007 2017], 0, 0; ...
            'lts',      [4], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'lts',      [4], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'lts',      [4], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
        };

    elseif hourToPlot == 16
        %% SEP 16Z
        somsForAnalysis = {...
            'z@500',    [16], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'z@500',    [16], 'May',            [2007 2017], 0, 0; ...
            'z@500',    [16], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@500',    [16], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'z@500',    [16], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'z@850',    [16], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'z@850',    [16], 'May',            [2007 2017], 0, 0; ...
            'z@850',    [16], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'z@850',    [16], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'z@850',    [16], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'mslp',     [16], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'mslp',     [16], 'May',            [2007 2017], 0, 0; ...
            'mslp',     [16], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'mslp',     [16], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'mslp',     [16], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'eis',      [16], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'eis',      [16], 'May',            [2007 2017], 0, 0; ...
            'eis',      [16], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'eis',      [16], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'eis',      [16], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
            'blh',      [16], 'Jan-Feb',        [2008 2017], 0, 0; ...
            'blh',      [16], 'May',            [2007 2017], 0, 0; ...
            'blh',      [16], 'Apr-May-Jun',    [2007 2017], 0, 1; ...
            'blh',      [16], 'Aug-Sep',        [2007 2017], 0, 1; ...
            'blh',      [16], 'Oct-Nov-Dec',    [2007 2017], 0, 1; ...
        };
    end
end

if ~exist('somsForAnalysis','var')
    error(sprintf('No somsForAnalysis found for %s %iZ', somRegion, hourToPlot))
end