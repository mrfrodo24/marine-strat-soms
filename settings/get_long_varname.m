function [longname] = get_long_varname(varname, varargin)
%GET_LONG_VARNAME Get the full variable name (with units) based on
%simplified varname for figure title in a projection plot
%
% INPUTS
%	varname:	string
%	varargin:
%		(1) -> set to 1 to remove units from longname

[varname, level] = parse_varname_level(varname,1);
switch varname
    case 'mslp',            longname = 'Mean sea level pressure (mb)';
    case 'eis',             longname = 'Estimated inversion strength (K)';
    case 'lts',             longname = 'Lower-tropospheric stability (K)';
    case 'blh',             longname = 'Boundary layer height (m)';
    case 'blh-rh',          longname = 'Boundary layer height @ 70% RH (m)';
    case 'sst',             longname = 'Sea-surface temperature (K)';
    case 'z',               longname = [level ' geopotential heights (m)'];
    case 'rh',              longname = [level ' relative humidity (%)'];
    case 'u',               longname = [level ' U-wind (m/s)'];
    case 'v',               longname = [level ' V-wind (m/s)'];
    case 'div',             longname = [level ' divergence (s^-^1)'];
    case 'blh-ew-grad',     longname = 'Zonal gradient of BLH (m/km)';
    case 'wind-vec',        longname = [level ' wind vectors'];
    case 'modis-lwp',       longname = 'MODIS LWP (g/m^2)';
    case 'modis-peakLwc',   longname = 'MODIS Peak LWC (g/kg)';
    otherwise
        error('Could not find matching varname for longer version.')
end

if nargin >= 2 && varargin{1}
	longname = longname(1 : strfind(longname, '(')-2);
end

end

