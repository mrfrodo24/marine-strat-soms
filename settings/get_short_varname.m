function [shortname] = get_short_varname(varname)
%GET_LONG_VARNAME Get abbreviated version of variable name
%based on the '%s@%s' format.

switch varname
    % add cases here if variable has different abbreviation than identifier
	case 'modis-peakLwc'
		shortname = 'peak LWC';
	case 'modis-lwp'
		shortname = 'LWP';
	case 'modis-lwc'
		shortname = 'LWC';
    otherwise
        shortname = upper(varname);
end

end

