%% netCDF data

% path to netCDF data
datapath = '/home/disk/ivanova2/spencerwork/thesis/data/ECMWF/SEA_largescale_5to35S/sfc/';
%datapath = 'C:\Users\rhodess\Documents\CP3G_Matlab\marine-strat-tools\data\ECMWF';

% specify the netCDF file to get data from
ncfile = 'era5_largescale_SEA_sfc_200701.nc';
% ncfile = 'era5_atlantic_largescale_2007_17.nc'; % ERA5 data on 850 and 500 mb levels over SEA
% ncfile = 'era5_largescale_2007_17_SEA_sfc.nc';  % ERA5 data at sfc over SEA

% set to 1 to set structs for loading data from monthly ERA5 files
loadEra5ByMonth = 1;

% ERA5 data by month, both data at sfc and pressure level(s)
if loadEra5ByMonth
	if ~exist('region','var')
		region = 'SEA'; % default region
	end
	switch region
	case 'SEA'
		sfcData.folder = '/home/disk/ivanova2/spencerwork/thesis/data/ECMWF/SEA_largescale_5to35S/sfc/';
		plData.folder = '/home/disk/ivanova2/spencerwork/thesis/data/ECMWF/SEA_largescale_5to35S/pl/';
		sfcData.filemask = 'era5_largescale_SEA_sfc_*.nc';
		plData.filemask = 'era5_largescale_SEA_levels_*.nc';
	case 'SEP'
		sfcData.folder = '/home/disk/ivanova2/spencerwork/thesis/data/ECMWF/SEP_largescale_5to35S/sfc/';
		plData.folder = '/home/disk/ivanova2/spencerwork/thesis/data/ECMWF/SEP_largescale_5to35S/pl/';
		sfcData.filemask = 'era5_largescale_SEP_sfc_*.nc';
		plData.filemask = 'era5_largescale_SEP_levels_*.nc';
	end
end

% name in netCDF file of base variable to load
baseVarname = 'eis';

% level of base data to get (set to [] if no level specification necessary,
% e.g. for surface data)
baseLevel = [];

%% Filtering

% only load data from these specified months of each year (as integers, where 1 is January)
whichMonths = [5];

% only load data from these specified hours of each day
whichHours = [22]; %#ok<NBRAK>

% When comparing data from single hour each day to cloud-eroding boundary
% classification, may want to be comparing to the following day's
% classification. Set to 1 to enable this.
addDayForBoundaryComparison = 1;

% when doing anomalies from a running average, do it for an average of the
% following duration (in days) centered around each day.  Hence, the last
% lengthOfMean/2 and first lengthOfMean/2 days will be NaNs.
lengthOfMean = 10; % days

%% sompak (SOM utilities)

sompakPath = '/home/disk/ivanova2/spencerwork/thesis/sompak';

%% Marine-strat-tools

% path to local repo
% marineStratToolsPath = 'C:\Users\rhodess\Documents\CP3G_Matlab\marine-strat-tools\';
marineStratToolsPath = '/home/disk/ivanova2/spencerwork/MATLAB/marine-strat-tools/';

% general data for marine stratocumulus research (incl. boundary
% classification)
% marineStratDataPath = 'C:\Users\rhodess\Documents\CP3G_Matlab\marine-strat-data\';
marineStratDataPath = '/home/disk/ivanova2/spencerwork/thesis/data/';

% cloud-eroding boundary classification CSV
boundaryClassFile = 'boundary_classification_2007_2017.csv';

%% SOM

% SOM dimensions (rows by columns)
somDims = [5 5];

% SOM base filename. This script outputs the result from som_make as an
% intermediary result. This variable specifies the filename.
somBaseFilename = 'som_3x4_base_eis_May_2007_17';

% Title for plot of base SOM
somBasePlotTitle = 'Estimated inversion strength (EIS) anomalies SOM (22 UTC in May, 2007-2017)';
% Suffix for projection plots
somProjectionTitleSuffix = ' based on EIS SOM (22 UTC in May, 2007-2017)';

% Set to 1 to flip the codebook (more for comparison than anything)
flipCodebook = 0;
