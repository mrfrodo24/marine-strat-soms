%% Baseline values for running SOM configuration
%
% Variables herein are a subset of user_params.m that won't vary based on
% region, hours to process, etc.

% sompakPath = 'C:\Users\rhodess\Documents\CP3G_Matlab\sompak';
sompakPath = '/home/disk/ivanova2/spencerwork/thesis/sompak';

% marineStratToolsPath = 'C:\Users\rhodess\Documents\CP3G_Matlab\marine-strat-tools\';
marineStratToolsPath = '/home/disk/ivanova2/spencerwork/MATLAB/marine-strat-tools/';

% marineStratDataPath = 'C:\Users\rhodess\Documents\CP3G_Matlab\marine-strat-data\';
marineStratDataPath = '/home/disk/ivanova2/spencerwork/MATLAB/marine-strat-data/';

boundaryClassFile = 'boundary_classification_2007_2017.csv';

flipCodebook = 0;

baseFilePrefix = sprintf('som_%ix%i_base', somDims(1), somDims(2));

% default length of centered mean for doing normalized anomalies is 10 days
lengthOfMean = 10;