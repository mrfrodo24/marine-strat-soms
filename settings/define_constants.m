%% add paths
project_dirs = {...
    'data', ...
    'helpers', ...
    'io', ...
    'transformers' ...
};
for i = 1:length(project_dirs)
    project_dir = [pwd filesep project_dirs{i}];
    if isempty(strfind(path, project_dir)) %#ok<STREMP>
        addpath(project_dir);
    end
end


%% netCDF constants
% varname of latitudes in netCDF file
LATITUDE = 'latitude';
% varname of longitudes in netCDF file
LONGITUDE = 'longitude';

% construct file path based on user_params
if exist('datapath', 'var') && ~exist('ncfilepath', 'var')
    if datapath(end) ~= '/' && datapath(end) ~= '\'
        datapath(end+1) = filesep;
    end
    ncfilepath = [datapath ncfile];
end

