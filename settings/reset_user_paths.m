function [ varargout ] = reset_user_paths( host, region, firstFile )
%RESET_USER_PATHS Update paths to important data and/or utilities based on
%the host machine.
%
% INPUTS
%   host:   An identifier for the host machine and the paths associated
%           with that machine.
%
% OUTPUTS (variable # of arguments in case need to add more)
%   (1) =   datapath
%   (2) =   ncfile
%   (3) =   sfcData
%   (4) =   plData
%   (5) =   sompakPath
%   (6) =   marineStratToolsPath
%   (7) =   marineStratDataPath
%

switch host
    case 'rhodess'
        % Spencer Rhodes's local environment
        varargout{1} = 'C:\Users\rhodess\Documents\CP3G_Matlab\marine-strat-data\ECMWF\';
        varargout{2} = sprintf('era5_largescale_%s_sfc_%s.nc', region, firstFile);
        varargout{3} = []; % doesn't have data by month
        varargout{4} = []; % doesn't have data by month
        varargout{5} = 'C:\Users\rhodess\Documents\CP3G_Matlab\sompak';
        varargout{6} = 'C:\Users\rhodess\Documents\CP3G_Matlab\marine-strat-tools\';
        varargout{7} = 'C:\Users\rhodess\Documents\CP3G_Matlab\marine-strat-data\';
        
    case 'ea-server'
        % One of the environment analytics servers (molari, ivanova, ...)
        varargout{1} = sprintf('/home/disk/ivanova2/spencerwork/thesis/data/ECMWF/%s_largescale_5to35S/sfc/', region);
        varargout{2} = sprintf('era5_largescale_%s_sfc_%s.nc', region, firstFile);
        varargout{3} = struct(...
            'folder', sprintf('/home/disk/ivanova2/spencerwork/thesis/data/ECMWF/%s_largescale_5to35S/sfc/', region), ...
            'filemask', sprintf('era5_largescale_%s_sfc_*.nc', region) ...
        );
        varargout{4} = struct(...
            'folder', sprintf('/home/disk/ivanova2/spencerwork/thesis/data/ECMWF/%s_largescale_5to35S/pl/', region), ...
            'filemask', sprintf('era5_largescale_%s_levels_*.nc', region) ...
        );
        varargout{5} = '/home/disk/ivanova2/spencerwork/thesis/sompak';
        varargout{6} = '/home/disk/ivanova2/spencerwork/MATLAB/marine-strat-tools/';
        varargout{7} = '/home/disk/ivanova2/spencerwork/thesis/data/';
        
    otherwise
        error('Specified host not found.')
        
end

end

