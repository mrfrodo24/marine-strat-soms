function [ som_var ] = reshape_3D_field_to_2D( field )
%RESHAPE_FIELD Reshape a 3D field into a 2D array for SOM analysis
%   
%   INPUTS:
%       field - Array of data to reshape (must be 3-D, longitude x latitude x time)
%
%   OUTPUT:
%       Data is reshaped such that converts each column into a row and
%       concatenates them.

lonSize = size(field,1);
latSize = size(field,2);
numRecords = size(field,3);
numLocations = lonSize * latSize;
som_var = nan(numRecords, lonSize * latSize);
for i = 1 : numRecords
    som_var(i,:) = reshape(field(:,:,i), [1 numLocations]);
end

end

