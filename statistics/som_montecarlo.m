function [yesPerNodeMean, noPerNodeMean, yesPerNodeRange, noPerNodeRange] = ...
    som_montecarlo(hits, yesHits, noHits, varargin) 
%Monte Carlo testing for significance of SOM cross-referenced boundary days
%
% SUMMARY
%	For a given set of SOM results, we know the hits for each node, so we have
%	an expected probability of a day falling into each node.
%
%	As well, for a given time period, we know the # of yes and no days, so
%	for 10,000 iterations, we will do the following:
%
%	1) Take the yes and no days which belong in the 25 nodes
%	2) Randomly prescribe the yes's and no's into a node, maintaing the
%		probability of each node based on its hits
%	3) At the end, record # of yes's and no's in each node
%
%	After 10,000 iterations, we can see the average # of yes's and no's for 
%	each node and the standard deviations for that.  Then, comparing
%	back to the actual yes's and no's in each node, see which nodes have
%	a significant number of yes's or no's.  Mostly looking for ones with
%	more than expected, but also less than expected.
% 
% INPUTS
%   hits:       vector specifying the # of hits/days in each node.
%   yesHits:    vector specifying the # of yes days in each node.
%   noHits:     vector specifying the # of no days in each node.
%   varargin:
%       (1) percentileInt - the percentile interval to use for
%           yesPerNodeRange and noPerNodeRange. Default is 5 => 95% and 5%
%           percentiles.
%
% OUTPUTS
%   yesPerNodeMean:     the average # of randomly assigned yes days per node.
%   noPerNodeMean:      the average # of randomly assigned no days per node.
%   yesPerNodeRange:    the integer percentiles of randomly assigned yes
%                       days per node.
%   noPerNodeRange:     the integer percentiles of randomly assigned no
%                       days per node.

percentileInt = 5;
if length(varargin) >= 1 && isscalar(varargin{1})
    percentileInt = varargin{1};
end

numIterations = 10000;
numNodes = length(hits);
totalHits = sum(hits);
totalYes = sum(yesHits);
totalNo = sum(noHits);
hitArray = [];
for h = 1 : length(hits)
	hitArray(end+1:end+hits(h)) = h;
end

rng('shuffle'); % shuffle random number generator seed
randomNo = nan(numIterations,totalNo);
randomYes = nan(numIterations,totalYes);
for i = 1 : numIterations
    randomNo(i,:) = hitArray(ceil(rand(totalNo,1) * totalHits));
    randomYes(i,:) = hitArray(ceil(rand(totalYes,1) * totalHits));
end

yesPerIterationPerNode = nan(numIterations,numNodes);
noPerIterationPerNode = nan(numIterations,numNodes);
for n = 1 : numNodes
    yesPerIterationPerNode(:,n) = sum(randomYes == n,2);
    noPerIterationPerNode(:,n) = sum(randomNo == n,2);
end

yesPerNodeRange = prctile(yesPerIterationPerNode,abs(percentileInt-[100 0]),1);
noPerNodeRange = prctile(noPerIterationPerNode,abs(percentileInt-[100 0]),1);
yesPerNodeMean = mean(yesPerIterationPerNode,1);
noPerNodeMean = mean(noPerIterationPerNode,1);

end